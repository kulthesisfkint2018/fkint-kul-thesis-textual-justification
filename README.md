# Textual Justification of Neural Network tactics for the game Ms. Pac-Man

## Agent models
See `experiments/agents`.
The model being used throughout the remainder of this repository was trained by ppwwyyxx (https://github.com/ppwwyyxx/tensorpack/tree/master/examples/A3C-Gym).
It should be downloaded from `http://models.tensorpack.com/OpenAIGym/MsPacman-v0.tfmodel` and stored at `lib/tensorpack/resources/models/MsPacman-v0.tfmodel`.

## Visual attribution
See `experiments/visual_attribution`.

## Pipeline
### Generate unlabelled datasets
The code to generate unlabelled datasets can be found in the `training_data` package. The `collect_data.py` script runs with default settings which can be modified.

Example command:
`python -m training_data.collect_data --count 10 --path PATH`.
This samples 10 situations in the game and stores the raw observation (numpy array) into `PATH/images/*.npy` files. The metadata of each of these events is stored in `PATH/metadata.json`. 

### Import unlabelled datasets into the labelling interface
Load the dataset into the web UI (requires that the raw data is kept in the same location on the file system).
Command: `FLASK_APP=web_server/web.py flask import_dataset --metadata_path PATH/metadata.json --dataset_name NAME`.
This creates a database entry to the server's database.

### Use the labelling interface to collect labels
Set up the web server configuration (`web_server/config.cfg`).
Run the web server with `FLASK_DEBUG=1 FLASK_APP=web_server/web.py flask run`.
List the datasets at `http://localhost:5000/datasets`.

Create a user with `FLASK_APP=web_server/web.py flask create_user --username USERNAME` and choose a password.
To label, you need to log in with that user account.

Go to `http://localhost:5000/datasets` to see an overview of the available datasets. If logged in, the website will show which datasets have been labelled and which ones haven't.
Click on a dataset and click 'label' to start annotating the game states.


### Export labelled datasets into the justification generator
The command `FLASK_APP=web_server/web.py FLASK_DEBUG=1 flask export_labelled_data --dataset_id 3 --user_id 1 $PATH` 
creates a `vocabulary` and a `data.tfrecords` file in the `PATH` directory. You need to pass the `PATH` to the jupyter 
notebook when loading training or test data.

## Text generation experiments
See `experiments/text_generation`