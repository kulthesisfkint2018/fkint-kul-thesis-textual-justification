import matplotlib.pyplot as plt

DATA = [
    ("Dueling DQN", "./baselines_deepq/evaluation_100.csv"),
    ("A3C LSTM", "./plain_a3c/evaluation_100.csv"),
    ("A2C", "./pytorch_a2c/evaluation_100.csv"),
    ("ACKTR (5 steps)", "./pytorch_acktr_05/evaluation_100.csv"),
    ("ACKTR (20 steps)", "./pytorch_acktr_20/evaluation_100.csv"),
    ("ACKTR (50 steps)", "./pytorch_acktr_50_10e7/evaluation_100.csv"),
    ("ACKTR (100 steps)", "./pytorch_acktr_100_10e7/evaluation_100.csv"),
    ("A3C FF (pre-trained)", "./tensorpack_a3c/evaluation_100.csv")
]


def get_data_points(filename):
    with open(filename, 'r') as f:
        return list(map(float, f.readlines()))


plt.figure()
data = [get_data_points(f[1]) for f in DATA]
data_labels = [f[0] for f in DATA]
plt.boxplot(data, labels=data_labels)
plt.xticks(rotation=45)

plt.ylabel('Rewards')
plt.title("Performance of different models on MsPacmanNoFrameskip-v0")
plt.tight_layout()
plt.show()

plt.savefig('performance_comparison.png')
