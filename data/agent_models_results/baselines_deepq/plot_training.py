import sys

import matplotlib.pyplot as plt
import numpy as np

sys.stdin.readline()
scores = dict(map(float, l.strip().split(',')[1:]) for l in sys.stdin.readlines())

sorted_keys = list(sorted(scores.keys()))
sorted_scores = list(scores[x] for x in sorted_keys)


def get_data(keys, scores):
    n = 5
    new_scores = (np.cumsum(scores)[n:] - np.cumsum(scores)[:-n]) / n
    return keys[n:], new_scores


num_steps = max(sorted_keys)

fig = plt.figure()
data_keys, data_scores = get_data(sorted_keys, sorted_scores)
plt.plot(data_keys, data_scores, label="Dueling DQN with Prioritized Experience Replay")

tick_fractions = np.array([0, .2, .4, .6, .8, 1.0])
ticks = tick_fractions * num_steps

tick_names = ["{:.1e}".format(tick) for tick in ticks]
plt.xticks(ticks, tick_names)
plt.xlim(0, num_steps * 1.01)
plt.xlabel('Number of Timesteps')
plt.ylabel('Rewards')

plt.title("MsPacmanNoFrameskip-v0")
plt.legend(loc=4)
plt.show()

plt.savefig('training.png')
