# DIRECTION_CODES based on https://gist.github.com/ajschumacher/c5d441e5014527ddab679f2094c709b3

# can be checked using env.get_action_meanings()
DIRECTION_CODES = {
    1: 'UP',
    2: 'RIGHT',
    3: 'LEFT',
    4: 'DOWN',
    5: 'UP-RIGHT',
    6: 'UP-LEFT',
    7: 'DOWN-RIGHT',
    8: 'DOWN-LEFT',
    0: 'NOTHING'
}
