import moviepy.editor as mpy
import numpy as np
import scipy.misc


def make_gif(images, fname, duration, true_image=True):
    def make_frame(t):
        try:
            x = images[int(len(images) / duration * t)]
        except:
            x = images[-1]
        if true_image:
            return x.astype(np.uint8)
        else:
            return ((x + 1) / 2 * 255).astype(np.uint8)

    clip = mpy.VideoClip(make_frame, duration=duration)
    clip.write_gif(fname, fps=len(images) / duration, verbose=False)


def make_black_frame(frame):
    return frame * 0


def get_justification_overlay_frame(image, justification, filter_fn=None,
                                    overlay_fn=None):
    image = observation_to_image(image)
    if filter_fn is None:
        filter_fn = lambda x: x > np.percentile(x, 90)
    enabled = filter_fn(justification)
    three_layers_filtered = np.reshape(np.repeat(enabled, 3, -1), image.shape)
    black_white = np.vectorize(lambda b: 255 if b else 0)(three_layers_filtered)
    if overlay_fn is None:
        overlay = np.vectorize(lambda b: 1 if b else .5)(three_layers_filtered)
    else:
        overlay = overlay_fn(justification)
        overlay = np.reshape(np.repeat(overlay, 3, -1), image.shape)
    image_with_overlay = np.multiply(overlay, image)
    stacked = np.concatenate([image, black_white, image_with_overlay], axis=1)
    return stacked


def split_observation(observation):
    return [
        observation[..., 3 * i:3 * (i + 1)]
        for i in range(observation.shape[-1] // 3)
    ]


def observation_to_image(ob):
    return ob[:, :, -3:]


def write_np_to_image(data, path):
    scipy.misc.toimage(data, cmin=0, cmax=1).save(path)
