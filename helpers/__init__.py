class ModelConfiguration(object):
    def __init__(self, path):
        self.path = path


DEFAULT_MODEL_CONFIGURATION = ModelConfiguration('./lib/tensorpack/resources/models/MsPacman-v0.tfmodel')
