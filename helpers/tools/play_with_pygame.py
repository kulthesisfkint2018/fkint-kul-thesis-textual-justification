# Source: https://gist.github.com/ajschumacher/c5d441e5014527ddab679f2094c709b3
import time

import gym
import pygame

frame_time = 1.0 / 15  # seconds

pygame.init()

env = gym.make('MsPacmanNoFrameskip-v0')
# env.frameskip = 3
env.reset()

# directions mapped on keyboard
actions = {'u': 6, 'i': 1, 'o': 5,
           'j': 3, 'k': 0, 'l': 2,
           'm': 8, ',': 4, '.': 7}
actions = {ord(key): value
           for key, value in actions.items()}

then = 0
done = False
last_key = ord('k')
total_reward = 0
while True:
    now = time.time()
    if frame_time < now - then:
        if not done:
            print('action: {}'.format(last_key))
            _, rew, done, _ = env.step(actions[last_key])
            total_reward += rew
            print(total_reward)
        else:
            env.reset()
            done = False
            total_reward = 0
        env.render()
        then = now
        last_key = ord('k')
    event = pygame.event.poll()
    if event.type == pygame.KEYDOWN:
        last_key = event.dict["key"]
    last_key = last_key if last_key in actions else ord('k')
