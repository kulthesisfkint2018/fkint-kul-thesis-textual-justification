import random

import cv2
import gym
import matplotlib
import numpy as np
import tensorflow as tf
from tensorpack.predict.base import OfflinePredictor
from tensorpack.predict.config import PredictConfig
from tensorpack.tfutils.sessinit import get_model_loader

from lib.tensorpack.atari_wrapper import MapState, FrameStack, FireResetEnv, LimitLength
from lib.tensorpack.model import OriginalAgentModel

matplotlib.use('TkAgg')


class ExtendedAgentModel(OriginalAgentModel):
    def __init__(self, num_actions, image_shape3):
        OriginalAgentModel.__init__(self, num_actions, image_shape3)

    def _build_graph(self, inputs):
        OriginalAgentModel._build_graph(self, inputs)
        self.policy_gradients = [tf.gradients(self.softmax_policy[:, i], self.input_image) for i in
                                 range(self.num_actions)]
        self.concatenated_gradients = tf.concat(self.policy_gradients, 0)  # action space dimension
        self.summed_gradients = tf.reduce_sum(self.concatenated_gradients, 1)  # batch dimension
        self.attribution = tf.identity(self.summed_gradients, name='input-attribution')


# Code based on tensorpack examples: https://github.com/ppwwyyxx/tensorpack/blob/master/examples/A3C-Gym/train-atari.py

IMAGE_SIZE = (84, 84)
FRAME_HISTORY = 4
CHANNEL = FRAME_HISTORY * 3
IMAGE_SHAPE3 = IMAGE_SIZE + (CHANNEL,)

ENV_NAME = "MsPacman-v0"


def get_player(train=False, dumpdir=None, env_id=ENV_NAME):
    env = gym.make(env_id)
    if dumpdir:
        env = gym.wrappers.Monitor(env, dumpdir)
    env = FireResetEnv(env)  # Does not do much, I think?
    env = MapState(env, lambda im: cv2.resize(im, IMAGE_SIZE))
    env = FrameStack(env, 4)
    if train:
        env = LimitLength(env, 60000)
    return env


class ExtendedAgent(object):
    def __init__(self, model_configuration):
        self.action_space = get_player().action_space
        num_actions = self.action_space.n
        # Use separate predictor for when input attribution is not needed
        self.predictor = OfflinePredictor(PredictConfig(
            model=ExtendedAgentModel(num_actions, IMAGE_SHAPE3),
            session_init=get_model_loader(model_configuration.path),
            input_names=['state'],
            output_names=self.get_output_names()))

    def get_output_names(self):
        raise NotImplemented("Need to override get_output_names")


class SimpleExtendedAgent(ExtendedAgent):
    def get_output_names(self):
        return ['policy']

    def predict(self, observation):
        return self.predictor(observation[None, :, :, :])[0][0].argmax()

    def predict_epsilon_greedy(self, observation, eps=0.01):
        act = self.predict(observation)
        if random.random() < eps:
            return self.action_space.sample()
        return act


class CNNExtendedAgent(ExtendedAgent):
    def __init__(self, model_configuration, output_tensor_name='fc0/output'):
        self.output_tensor_name = output_tensor_name
        super().__init__(model_configuration)

    def get_output_names(self):
        return ['policy', self.output_tensor_name]

    def predict(self, observation):
        result = self.predictor(observation[None, :, :, :])
        policy = result[0][0].argmax()
        fc0 = result[1][0]
        return policy, fc0


class InputAttributionExtendedAgent(ExtendedAgent):
    def __init__(self, model_configuration, num_samples):
        ExtendedAgent.__init__(self, model_configuration)
        self.num_samples = num_samples

    def get_output_names(self):
        return ['policy', 'input-attribution']

    def predict_simple(self, observation):
        """
        Use SimpleExtendedAgent whenever possible (much faster as the input attributions do not need to be computed every time).
        :param observation:
        :return:
        """
        return self.predictor(observation[None, :, :, :])[0][0].argmax()

    def predict(self, observation):
        """

        :param observation:
        :return: (average scaled attributions, action)
        """
        repeated_images = np.tile(observation[None, :, :, :], (self.num_samples, 1, 1, 1))
        scaled_images = np.multiply(repeated_images,
                                    (np.arange(.5 / self.num_samples, 1, 1. / (self.num_samples))

                                     ).reshape(
                                        (self.num_samples, 1, 1, 1)))

        res = self.predictor(scaled_images)
        results = res[0][:].argmax(axis=1)
        attributions = res[1]
        scaled_attributions = np.multiply(attributions, observation)
        per_pixel_scaled_attributions = np.sum(scaled_attributions, 3)
        action = results[-1]

        action_scores = np.array(res[0])

        binary_actions = np.multiply(
            np.tile(observation[None, :, :, :], (2, 1, 1, 1)),
            np.array([0, 1]).reshape((2, 1, 1, 1)))
        binary_scores, binary_attributions = self.predictor(binary_actions)
        score_diff = binary_scores[1] - binary_scores[0]
        sum_of_attributions_per_action = np.sum(per_pixel_scaled_attributions, axis=(1, 2))
        sum_of_attributions_over_delta_f = sum_of_attributions_per_action / score_diff / self.num_samples
        return per_pixel_scaled_attributions, action, action_scores, sum_of_attributions_over_delta_f
