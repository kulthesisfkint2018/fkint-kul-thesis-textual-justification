import tensorflow as tf
from tensorpack.graph_builder.model_desc import InputDesc, ModelDesc
from tensorpack.models.conv2d import Conv2D
from tensorpack.models.fc import FullyConnected
from tensorpack.models.nonlin import PReLU
from tensorpack.models.pool import MaxPooling
from tensorpack.tfutils import summary
from tensorpack.tfutils import symbolic_functions as symbf, optimizer
from tensorpack.tfutils.argscope import argscope
from tensorpack.tfutils.gradproc import MapGradient, SummaryGradient
from tensorpack.tfutils.tower import get_current_tower_context


class OriginalAgentModel(ModelDesc):
    """model description
    Based strongly on https://github.com/ppwwyyxx/tensorpack/blob/master/examples/A3C-Gym/train-atari.py
    Changes include storing references to the tensorflow operations

    To be implemented/overridden:
        ModelDescBase._get_inputs
        ModelDescBase._build_graph
        ModelDesc._get_optimizer
        ModelDesc.cost
    """

    def __init__(self, num_actions, image_shape3):
        self.num_actions = num_actions
        self.image_shape3 = image_shape3

    def _get_inputs(self):
        assert self.num_actions is not None
        return [InputDesc(tf.uint8, (None,) + self.image_shape3, 'state'),
                InputDesc(tf.int64, (None,), 'action'),
                InputDesc(tf.float32, (None,), 'futurereward'),
                InputDesc(tf.float32, (None,), 'action_prob'),
                ]

    def _get_NN_prediction(self, image):
        self.input_image = tf.cast(image, tf.float32)
        self.image = self.input_image / 255.0
        print('image shape: ', self.image.shape)
        with argscope(Conv2D, nl=tf.nn.relu):
            self.conv0 = Conv2D('conv0', self.image, out_channel=32, kernel_shape=5)
            self.maxp0 = MaxPooling('pool0', self.conv0, 2)
            self.conv1 = Conv2D('conv1', self.maxp0, out_channel=32, kernel_shape=5)
            self.maxp1 = MaxPooling('pool1', self.conv1, 2)
            self.conv2 = Conv2D('conv2', self.maxp1, out_channel=64, kernel_shape=4)
            self.maxp2 = MaxPooling('pool2', self.conv2, 2)
            self.conv3 = Conv2D('conv3', self.maxp2, out_channel=64, kernel_shape=3)

        self.fc0 = FullyConnected('fc0', self.conv3, 512, nl=tf.identity)
        self.prelu = PReLU('prelu', self.fc0)
        self.policy = FullyConnected('fc-pi', self.prelu, out_dim=self.num_actions,
                                     nl=tf.identity)  # unnormalized policy
        self.value = FullyConnected('fc-v', self.prelu, 1, nl=tf.identity)

        return self.policy, self.value

    def _build_graph(self, inputs):
        state, action, futurereward, action_prob = inputs
        logits, value = self._get_NN_prediction(state)

        value = tf.squeeze(value, [1], name='pred_value')  # (B,)
        self.softmax_policy = tf.nn.softmax(logits, name='policy')
        policy = self.softmax_policy
        is_training = get_current_tower_context().is_training
        if not is_training:
            return
        log_probs = tf.log(policy + 1e-6)

        log_pi_a_given_s = tf.reduce_sum(
            log_probs * tf.one_hot(action, self.num_actions), 1)
        advantage = tf.subtract(tf.stop_gradient(value), futurereward, name='advantage')

        pi_a_given_s = tf.reduce_sum(policy * tf.one_hot(action, self.num_actions), 1)  # (B,)
        importance = tf.stop_gradient(tf.clip_by_value(pi_a_given_s / (action_prob + 1e-8), 0, 10))

        policy_loss = tf.reduce_sum(log_pi_a_given_s * advantage * importance, name='policy_loss')
        xentropy_loss = tf.reduce_sum(policy * log_probs, name='xentropy_loss')
        value_loss = tf.nn.l2_loss(value - futurereward, name='value_loss')

        pred_reward = tf.reduce_mean(value, name='predict_reward')
        advantage = symbf.rms(advantage, name='rms_advantage')
        entropy_beta = tf.get_variable('entropy_beta', shape=[],
                                       initializer=tf.constant_initializer(0.01), trainable=False)
        self.cost = tf.add_n([policy_loss, xentropy_loss * entropy_beta, value_loss])
        self.cost = tf.truediv(self.cost,
                               tf.cast(tf.shape(futurereward)[0], tf.float32),
                               name='cost')
        summary.add_moving_summary(policy_loss, xentropy_loss,
                                   value_loss, pred_reward, advantage,
                                   self.cost, tf.reduce_mean(importance, name='importance'))

    def _get_optimizer(self):
        lr = tf.get_variable('learning_rate', initializer=0.001, trainable=False)
        opt = tf.train.AdamOptimizer(lr, epsilon=1e-3)

        gradprocs = [MapGradient(lambda grad: tf.clip_by_average_norm(grad, 0.1)),
                     SummaryGradient()]
        opt = optimizer.apply_grad_processors(opt, gradprocs)
        return opt
