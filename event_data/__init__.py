import json
import logging
import os
import random

import numpy as np

from helpers.model_wrapper import CNNExtendedAgent, get_player

logging.getLogger().setLevel(logging.DEBUG)


class TrainingDataCollector(object):
    def __init__(self, model_configuration):
        self.agent = CNNExtendedAgent(model_configuration)

    def _run_game_until_event(self, event_filter):
        env = get_player(train=False)
        ob = env.reset()
        event_filter.reset()
        is_over = False
        total_reward = 0
        while not is_over:
            act, processed_observation = self.agent.predict(ob)
            if event_filter.process_data(ob, act, total_reward):
                return ob, act, processed_observation
            ob, r, is_over, info = env.step(act)
            total_reward += r
        return None

    def get_event_data(self, event_filter):
        while True:
            res = self._run_game_until_event(event_filter)
            if res is not None:
                return res


class EventFilter(object):
    def __init__(self, next_filter=None):
        self.next_filter = next_filter

    def process_data(self, observation, action, total_reward):
        own_judgement = self._process_data_internal(observation, action, total_reward)
        if self.next_filter is None:
            next_judgement = True
        else:
            next_judgement = self.next_filter.process_data(observation, action, total_reward)
        return own_judgement and next_judgement

    def _process_data_internal(self, observation, action, total_reward):
        raise NotImplementedError

    def _reset_internal(self):
        pass

    def reset(self):
        self._reset_internal()
        if self.next_filter is not None:
            self.next_filter.reset()


class NewActionEventFilter(EventFilter):
    def __init__(self, next_filter=None):
        super(NewActionEventFilter, self).__init__(next_filter)
        self.previous_action = None

    def _process_data_internal(self, observation, action, total_reward):
        previous_action = self.previous_action
        self.previous_action = action
        return previous_action is not None and action != previous_action

    def _reset_internal(self):
        self.previous_action = None


class ProbabilisticEventFilter(EventFilter):
    def __init__(self, prob, next_filter=None):
        super(ProbabilisticEventFilter, self).__init__(next_filter)
        self.prob = prob

    def _process_data_internal(self, observation, action, total_reward):
        return random.random() < self.prob


class NonZeroPointsEventFilter(EventFilter):
    def _process_data_internal(self, observation, action, total_reward):
        return total_reward > 0


def generate_data_id(index):
    return index


def write_numpy_data(directory, data_id, observation):
    """
    Writes the exact numpy array to disk.

    :return:
    """
    observation_path = os.path.join(directory, "{:05d}.npy".format(data_id))
    np.save(observation_path, observation)
    return observation_path


def write_metadata(meta_file, data_id, action, raw_data_path, processed_observation_path):
    """
    Optimize: keep file open when appending.
    :param meta_file:
    :param data_id:
    :param action:
    :param raw_data_path:
    :param processed_observation_path:
    :return:
    """
    logging.info(
        'Writing metadata to %s: data id = %s, action = %s, raw_data_path = %s, processed_observation_path = %s',
        meta_file, data_id, action, raw_data_path, processed_observation_path)
    try:
        with open(meta_file, 'r') as f:
            current_data = json.load(f)
    except Exception:
        logging.error('Could not load metadata, starting from scratch. %s', meta_file)
        current_data = []
    with open(meta_file, 'w') as f:
        data = current_data + [{
            "id": data_id,
            "action": int(action),
            "raw_data_path": raw_data_path,
            "processed_observation_path": processed_observation_path
        }]
        json.dump(data, f)


def gather_training_data(model_configuration, num_samples=500, target_directory='./data/', event_filter=None):
    os.makedirs(target_directory, exist_ok=True)
    images_directory = os.path.join(target_directory, 'images')
    os.makedirs(images_directory, exist_ok=True)
    processed_observation_directory = os.path.join(target_directory, 'processed_observations')
    os.makedirs(processed_observation_directory, exist_ok=True)

    meta_file = os.path.join(target_directory, 'metadata.json')
    collector = TrainingDataCollector(model_configuration)
    for index in range(num_samples):
        observation, action, processed_observation = collector.get_event_data(event_filter)
        data_id = generate_data_id(index)
        logging.debug('Got action: %s', action)
        observation_path = write_numpy_data(images_directory, data_id, observation)
        processed_observation_path = write_numpy_data(processed_observation_directory, data_id, processed_observation)
        write_metadata(meta_file, data_id, action, observation_path, processed_observation_path)
