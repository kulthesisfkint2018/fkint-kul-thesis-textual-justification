import argparse

import event_data
from helpers import DEFAULT_MODEL_CONFIGURATION


def get_data_collection_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', help='path to store the dataset', default='./data/simple')
    parser.add_argument('--count', type=int, help='size of the datasets')
    args = parser.parse_args()
    return args


def collect_simple_training_data(path='./data/simple', count=10):
    event_filter = event_data.NewActionEventFilter(
        event_data.ProbabilisticEventFilter(.01, event_data.NonZeroPointsEventFilter()))
    event_data.gather_training_data(DEFAULT_MODEL_CONFIGURATION, count, path, event_filter)


if __name__ == "__main__":
    args = get_data_collection_args()
    collect_simple_training_data(args.path, args.count)
