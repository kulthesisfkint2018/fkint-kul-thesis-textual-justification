import argparse

from experiments.text_generation.dataset.helpers import *
from helpers.game_constants import DIRECTION_CODES

parser = argparse.ArgumentParser()
parser.add_argument('--input_dir', default='data/exported-labels/200-train-validation/original-data')


def analyze_labels(labels):
    print(len(labels), 'labels')
    nb_per_action = {dc: 0 for dc in DIRECTION_CODES.keys()}
    for l in labels:
        nb_per_action[l['action']] += 1
    for action, nb_examples in nb_per_action.items():
        print(DIRECTION_CODES[action], ':', nb_examples)


def analyze_dataset(directory):
    labels = read_original_dataset(directory)
    analyze_labels(labels)


def main():
    args = parser.parse_args()
    print('Training data:')
    analyze_dataset(os.path.join(args.input_dir, TRAIN_FOLDERNAME))
    print('Validation data:')
    analyze_dataset(os.path.join(args.input_dir, VALIDATION_FOLDERNAME))


if __name__ == "__main__":
    main()
