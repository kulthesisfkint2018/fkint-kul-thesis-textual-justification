import argparse
import json
import logging
import os
import time

import numpy as np
from sklearn.model_selection import train_test_split

from experiments.text_generation.dataset.helpers import OBSERVATION_FILENAME, ACTION_FILENAME, LABEL_FILENAME, \
    INDEX_FILENAME, TRAIN_FOLDERNAME, VALIDATION_FOLDERNAME
from labelling.models import Dataset, User, Label


def retrieve_all_labels(dataset_id, user_ids):
    dataset = Dataset.query.get(dataset_id)
    all_labels = []
    for user in map(User.query.get, user_ids):
        for data_instance in dataset.data_instances:
            label = Label.query.filter_by(user_id=user.id, data_instance_id=data_instance.id).first()
            if label is None:  # ignore unlabeled instances
                continue
            all_labels.append(label)
    logging.info('Found %d labels', len(all_labels))
    return all_labels


def export_label(label, path):
    os.mkdir(path)
    np.save(os.path.join(path, OBSERVATION_FILENAME), np.load(label.data_instance.raw_data_path))
    with open(os.path.join(path, LABEL_FILENAME), 'w') as f:
        f.write(label.label)
    with open(os.path.join(path, ACTION_FILENAME), 'w') as f:
        f.write("{}\n".format(label.data_instance.action))


def export_labels(labels, output_dir):
    os.mkdir(output_dir)
    paths = []
    for l in labels:
        p = "{:05d}".format(l.id)
        path = os.path.join(output_dir, p)
        paths.append(p)
        export_label(l, path)
    with open(os.path.join(output_dir, INDEX_FILENAME), 'w') as f:
        json.dump({
            "time": time.time(),
            "paths": paths
        }, f)


parser = argparse.ArgumentParser()
parser.add_argument('--dataset', type=int, default=1)
parser.add_argument('--user_ids', type=int, nargs='+', default=[1])
parser.add_argument('--output_dir', type=str, default='data/exported-labels/200-train-validation/original-data')
parser.add_argument('--validation_ratio', type=float, default=0.15)


def main():
    args = parser.parse_args()
    os.mkdir(args.output_dir)
    labels = retrieve_all_labels(args.dataset, args.user_ids)
    train_labels, validation_labels = train_test_split(labels, test_size=args.validation_ratio)
    export_labels(train_labels, os.path.join(args.output_dir, TRAIN_FOLDERNAME))
    export_labels(validation_labels, os.path.join(args.output_dir, VALIDATION_FOLDERNAME))


if __name__ == "__main__":
    main()
