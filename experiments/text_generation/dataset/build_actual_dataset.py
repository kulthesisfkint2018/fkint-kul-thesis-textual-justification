import argparse
import shutil
import time

import tensorflow as tf

from experiments.text_generation.dataset.helpers import *
from helpers import DEFAULT_MODEL_CONFIGURATION
from helpers.model_wrapper import CNNExtendedAgent
from labelling.data_processing import _floats_feature, _int64_feature, _int64_feature_list, _int64s_feature
from lib.im2txt import vocabulary
from text_generation.inputs import get_records_path, get_validation_records_path
from text_generation.vocabulary_builder import VocabularyBuilder, TextNormalizer, TextCharNormalizer


def build_vocabulary(labels, text_normalizer, filename):
    vocabulary_builder = VocabularyBuilder()
    for label in labels:
        vocabulary_builder.feed_sentence(text_normalizer.normalize_sentence(label['label']))
    with open(filename, 'w') as f:
        f.writelines(x + "\n" for x in vocabulary_builder.build_vocabulary_list())
    voc = vocabulary.Vocabulary(filename)
    return voc


def build_tf_example_from_label(observation, processed_observation, action, label):
    feature = tf.train.Features(feature={
        "processed_observation": _floats_feature(processed_observation),
        "selected_policy": _int64_feature(action),
        "observation_input": _int64s_feature(observation)
    })
    feature_list = tf.train.FeatureLists(feature_list={
        "caption_ids": _int64_feature_list(label)
    })
    return tf.train.SequenceExample(
        context=feature,
        feature_lists=feature_list)


def export_tensorflow_labels(labels, filename):
    with tf.python_io.TFRecordWriter(filename) as writer:
        for label in labels:
            example = build_tf_example_from_label(label['observation'], label['processed_observation'], label['action'],
                                                  label['label'])
            writer.write(example.SerializeToString())


def export_tensorflow_all(training_labels, validation_labels, directory, vocabulary_path):
    os.mkdir(directory)
    shutil.copy(vocabulary_path, directory)
    export_tensorflow_labels(training_labels, get_records_path(directory))
    export_tensorflow_labels(validation_labels, get_validation_records_path(directory))


def get_tensorname(name):
    if name == 'last-fully-connected':
        return 'fc0/output'
    raise Exception('tensorname {} not supported'.format(name))


def process_label(l, voc, text_normalizer, agent):
    processed_observation = agent.predict(l['observation'])[1]
    return {
        "label": voc.sentence_to_ids(text_normalizer.normalize_sentence(l['label'])),
        "action": l['action'],
        "processed_observation": processed_observation,
        "observation": np.reshape(l['observation'], 84 * 84 * 12),
        'identifier': l['identifier']
    }


def process_labels(labels, voc, text_normalizer, agent):
    return [process_label(l, voc, text_normalizer, agent) for l in labels]


def export_plain_label(label, directory):
    os.mkdir(directory)
    np.save(os.path.join(directory, OBSERVATION_FILENAME), label['observation'])
    np.save(os.path.join(directory, PROCESSED_OBSERVATION_FILENAME), label['processed_observation'])
    with open(os.path.join(directory, LABEL_ACTION_FILENAME), 'w') as f:
        json.dump({
            "label": label['label'],
            "action": label['action']
        }, f)


def export_plain_labels(labels, directory):
    os.mkdir(directory)
    paths = []
    for l in labels:
        paths.append(l['identifier'])
        path = os.path.join(directory, l['identifier'])
        export_plain_label(l, path)
    with open(os.path.join(directory, INDEX_FILENAME), 'w') as f:
        json.dump({
            "paths": paths,
            "time": time.time()
        }, f)


def export_plain_all(training_labels, validation_labels, directory, vocabulary_path):
    os.mkdir(directory)
    shutil.copy(vocabulary_path, directory)
    export_plain_labels(training_labels, os.path.join(directory, TRAIN_FOLDERNAME))
    export_plain_labels(validation_labels, os.path.join(directory, VALIDATION_FOLDERNAME))


parser = argparse.ArgumentParser()
parser.add_argument('--input_dir', default='data/exported-labels/200-train-validation/original-data')
parser.add_argument('--output_dir',
                    default='data/exported-labels/200-train-validation/data-with-vocabulary-and-processed-observation')
parser.add_argument('--vocabulary', default='word', choices=['word', 'char'])
parser.add_argument('--processed_observation', default='last-fully-connected', choices=['last-fully-connected'])


def main():
    args = parser.parse_args()
    os.mkdir(args.output_dir)
    if args.vocabulary == 'word':
        text_normalizer = TextNormalizer()
    else:
        text_normalizer = TextCharNormalizer()

    training_labels = read_original_dataset(os.path.join(args.input_dir, TRAIN_FOLDERNAME))
    validation_labels = read_original_dataset(os.path.join(args.input_dir, VALIDATION_FOLDERNAME))
    vocabulary_path = os.path.join(args.output_dir, VOCABULARY_FILENAME)
    voc = build_vocabulary(training_labels, text_normalizer, vocabulary_path)

    agent = CNNExtendedAgent(DEFAULT_MODEL_CONFIGURATION, get_tensorname(args.processed_observation))
    processed_training_labels = process_labels(training_labels, voc, text_normalizer, agent)
    processed_validation_labels = process_labels(validation_labels, voc, text_normalizer, agent)

    export_plain_all(processed_training_labels, processed_validation_labels,
                     os.path.join(args.output_dir, PLAIN_FOLDERNAME), vocabulary_path)
    export_tensorflow_all(processed_training_labels, processed_validation_labels,
                          os.path.join(args.output_dir, TENSORFLOW_FOLDERNAME), vocabulary_path)


if __name__ == "__main__":
    main()
