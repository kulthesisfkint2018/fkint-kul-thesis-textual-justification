import json
import os

import numpy as np

OBSERVATION_FILENAME = 'observation.npy'
LABEL_FILENAME = 'label'
ACTION_FILENAME = 'action'
INDEX_FILENAME = 'index.json'
TRAIN_FOLDERNAME = 'train'
VALIDATION_FOLDERNAME = 'validation'
VOCABULARY_FILENAME = 'vocabulary'
TENSORFLOW_FOLDERNAME = 'tensorflow'
PROCESSED_OBSERVATION_FILENAME = 'processed_observation.npy'
LABEL_ACTION_FILENAME = 'label_and_action.json'
PLAIN_FOLDERNAME = 'plain'


def read_original_sample(directory, identifier):
    with open(os.path.join(directory, ACTION_FILENAME), 'r') as f:
        action = int(f.readline())
    with open(os.path.join(directory, LABEL_FILENAME), 'r') as f:
        label = f.readline()
    observation = np.load(os.path.join(directory, OBSERVATION_FILENAME))
    return {
        "action": action,
        "label": label,
        "observation": observation,
        "identifier": identifier
    }


def read_original_dataset(directory):
    with open(os.path.join(directory, INDEX_FILENAME), 'r') as f:
        index = json.load(f)
    return [read_original_sample(os.path.join(directory, p), p) for p in index['paths']]
