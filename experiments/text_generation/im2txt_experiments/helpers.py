import os

import tensorflow as tf

from helpers.game_constants import DIRECTION_CODES
from lib.im2txt.vocabulary import Vocabulary
from text_generation.inference import sentence_inference_with_beam_search, test_sentence_inference
from text_generation.inputs import get_vocabulary_path
from text_generation.inputs import make_training_input_fn, get_records_path, get_validation_records_path, \
    get_record_features
from text_generation.models.model import TextGenerationModel
from text_generation.vocabulary_builder import TextNormalizer, TextCharNormalizer


def get_estimator(params, model_dir):
    return tf.estimator.Estimator(model_fn=TextGenerationModel.get_model_fn(),
                                  params=params,
                                  model_dir=model_dir)


def get_params(vocabulary, optimizer="SGD", clip_gradients=5.0, learning_rate=2.0, initializer_scale=0.08,
               lstm_configuration=({"lstm_size": 512, "lstm_dropout_keep_prob": .7},), vocabulary_size=1200,
               embedding_size=512, ignore_observation=False, ignore_action=False, batch_size=32, num_epochs_per_decay=5,
               learning_rate_decay_factor=.5, l2_regularization_factor=0, num_examples_per_epoch=170
               ):
    return {
        "optimizer": optimizer,
        "clip_gradients": clip_gradients,
        "learning_rate": learning_rate,
        "initializer_scale": initializer_scale,
        "lstm_configuration": lstm_configuration,
        "vocabulary_size": vocabulary_size,
        "embedding_size": embedding_size,
        "ignore_observation": ignore_observation,
        "ignore_action": ignore_action,
        "batch_size": batch_size,
        "num_epochs_per_decay": num_epochs_per_decay,
        "num_examples_per_epoch": num_examples_per_epoch,
        "learning_rate_decay_factor": learning_rate_decay_factor,
        "l2_regularization_factor": l2_regularization_factor,
        "vocabulary": vocabulary
    }


def train_and_evaluate(estimator, params, training_steps_per_session, training_data_dir, evaluation_data_dir, voc,
                       nb_training_sessions=1, validation_steps=30):
    training_input_fn = make_training_input_fn([get_records_path(training_data_dir)], voc, params['batch_size'])
    validation_input_fn = make_training_input_fn([get_validation_records_path(evaluation_data_dir)], voc)
    evaluation_losses = []
    evaluation_steps = []
    for it in range(nb_training_sessions):
        print('Training iteration:', it + 1)
        estimator.train(
            input_fn=training_input_fn,
            steps=training_steps_per_session)

        result = estimator.evaluate(input_fn=validation_input_fn, steps=validation_steps)
        print("Evaluation: {}".format(result))
        evaluation_losses.append(result['loss'])
        evaluation_steps.append(result['global_step'])
    print('Evaluation losses: ', evaluation_losses)
    print('Evaluation steps: ', evaluation_steps)


def demonstrate_performance(estimator, params, data_dir, voc, text_normalizer):
    actions = []
    beam_sentences = []
    argmax_sentences = []
    for data in get_record_features(get_validation_records_path(data_dir)):
        features = data[0]
        action = DIRECTION_CODES[int(features["selected_policy"][0])]
        actions.append(action)
        beam_sentence = sentence_inference_with_beam_search(estimator, voc, text_normalizer, features, params,
                                                            word_hooks=[])
        argmax_sentence = test_sentence_inference(estimator, voc, text_normalizer, features, params)
        beam_sentences.append(beam_sentence)
        argmax_sentences.append(argmax_sentence)
        print("Got (BEAM)   {}: {}".format(action, beam_sentence))
        print("Got (ARGMAX) {}: {}".format(action, argmax_sentence))
    # TODO: statistics (how many literally in training data, using hooks for direction words)
    for a, beam_sentence, argmax_sentence in zip(actions, beam_sentences, argmax_sentences):
        print("ACTION", a)
        print("argmax sentence:", argmax_sentence)
        print("beam sentence:  ", beam_sentence)


def run_with_settings(input_dir, output_dir,
                      demonstrations, training_sessions_per_demonstration, steps_per_session,
                      char_level, param_settings):
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    tf.logging.set_verbosity(tf.logging.INFO)

    vocabulary_path = get_vocabulary_path(input_dir)
    voc = Vocabulary(vocabulary_path)
    if char_level:
        text_normalizer = TextCharNormalizer()
    else:
        text_normalizer = TextNormalizer()
    params = get_params(voc, **param_settings)
    estimator = get_estimator(params, output_dir)

    for i in range(demonstrations):
        print('DEMO', i, '/', demonstrations)
        train_and_evaluate(estimator, params, steps_per_session, input_dir, input_dir, voc,
                           nb_training_sessions=training_sessions_per_demonstration)

        demonstrate_performance(estimator, params, input_dir, voc, text_normalizer)
