from experiments.text_generation.im2txt_experiments.helpers import run_with_settings

INPUT_DIR = 'data/exported-labels/200-train-validation/word-last-fc/tensorflow'
OUTPUT_DIR = 'data/text_generation_models/200-train-validation/im2txt-adam-ignore-observation'
DEMONSTRATIONS = 10
TRAINING_SESSIONS_PER_DEMO = 10
STEPS_PER_SESSION = 1000
CHAR_LEVEL = False


def main():
    EXTRA_SETTINGS = {'optimizer': 'Adam', 'learning_rate': 0.001, 'learning_rate_decay_factor': 0,
                      'embedding_size': 128, 'ignore_observation': True, 'vocabulary_size': 350}
    run_with_settings(INPUT_DIR, OUTPUT_DIR, DEMONSTRATIONS, TRAINING_SESSIONS_PER_DEMO, STEPS_PER_SESSION, CHAR_LEVEL,
                      EXTRA_SETTINGS)


if __name__ == "__main__":
    main()
