from experiments.text_generation.im2txt_experiments.helpers import run_with_settings

INPUT_DIR = 'data/exported-labels/200-train-validation/word-last-fc/tensorflow'
OUTPUT_DIR = 'data/text_generation_models/200-train-validation/im2txt-ignore-observation'
DEMONSTRATIONS = 10
TRAINING_SESSIONS_PER_DEMO = 10
STEPS_PER_SESSION = 1000
CHAR_LEVEL = False
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--limit_vocabulary', action='store_true')
parser.set_defaults(limit_vocabulary=False)


def main():
    EXTRA_SETTINGS = {'ignore_observation': True}
    args = parser.parse_args()
    if args.limit_vocabulary:
        EXTRA_SETTINGS['vocabulary_size'] = 350
    run_with_settings(INPUT_DIR, OUTPUT_DIR, DEMONSTRATIONS, TRAINING_SESSIONS_PER_DEMO, STEPS_PER_SESSION, CHAR_LEVEL,
                      EXTRA_SETTINGS)


if __name__ == "__main__":
    main()
