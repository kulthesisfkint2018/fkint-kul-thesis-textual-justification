import argparse

from experiments.text_generation.keras_experiments.helpers import run_with_settings

ARCHITECTURE = 'merge'  # 'merge' or 'pre-inject'
INPUT_DIR = 'data/exported-labels/200-train-validation/word-last-fc/plain'
OUTPUT_DIR = 'data/text_generation_models/200-train-validation/keras_merge_im2txt_settings'
DEMONSTRATIONS = 100
EPOCHS_PER_SESSION = 10
CHAR_LEVEL = False

parser = argparse.ArgumentParser()
parser.add_argument('--load_model', default=None)


def main():
    args = parser.parse_args()
    EXTRA_SETTINGS =  {'vocabulary_size': 350, 'embedding_size': 128}
    run_with_settings(ARCHITECTURE, INPUT_DIR, OUTPUT_DIR, DEMONSTRATIONS, EPOCHS_PER_SESSION, CHAR_LEVEL,
                      EXTRA_SETTINGS, args.load_model)


if __name__ == "__main__":
    main()
