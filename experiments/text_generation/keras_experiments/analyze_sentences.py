import argparse

from experiments.text_generation.dataset.helpers import read_original_dataset
from helpers.game_constants import DIRECTION_CODES
from lib.im2txt.vocabulary import Vocabulary
from text_generation.inference import get_sentence_string
from text_generation.vocabulary_builder import TextNormalizer, TextCharNormalizer

parser = argparse.ArgumentParser()
parser.add_argument('--input', required=True)
parser.add_argument('--vocabulary', required=True)
parser.add_argument('--training_dataset', required=True)
parser.add_argument('--evaluated_dataset', required=True)
parser.add_argument('--char', action='store_true')


def main():
    args = parser.parse_args()

    with open(args.input, 'r') as f:
        sentences = f.readlines()
    voc = Vocabulary(args.vocabulary)

    if args.char:
        text_normalizer = TextCharNormalizer()
    else:
        text_normalizer = TextNormalizer()

    training_dataset = read_original_dataset(args.training_dataset)
    real_dataset = read_original_dataset(args.evaluated_dataset)

    dataset_sentences = [l['label'] for l in training_dataset]
    normalized_dataset_sentences = [tuple(voc.sentence_to_ids(text_normalizer.normalize_sentence(l))) for l in
                                    dataset_sentences]
    original_sentences = [tuple(voc.sentence_to_ids(text_normalizer.normalize_sentence(l['label']))) for l in
                                    real_dataset]
    normalized_input_sentences = [tuple(voc.word_to_id(w) for w in text_normalizer.sentence_splitter(s)) for s in
                                  sentences]
    print("Normalized input sentences:", len(normalized_input_sentences))
    print("Normalized database sentences:", len(normalized_dataset_sentences))
    print("Unique input sentences:", len(set(normalized_input_sentences)))
    sentences_not_in_dataset = [s for s in normalized_input_sentences if s not in normalized_dataset_sentences]
    print("Input sentences not appearing in dataset (duplicates counted):", len(sentences_not_in_dataset))
    print("Input sentences not appearing in dataset (duplicates not counted):", len(set(sentences_not_in_dataset)))
    print("UNIQUE NEW SENTENCES:")
    print("\n".join([get_sentence_string(ids, voc, text_normalizer) for ids in set(sentences_not_in_dataset)]))
    print("SENTENCES WITH OCCURRENCES:")
    for s in set(normalized_input_sentences):
        print("{}: {}".format(len([x for x in normalized_input_sentences if x == s]),
                              get_sentence_string(s, voc, text_normalizer)))
    sentences_per_action = {
        dc: dict() for dc in DIRECTION_CODES
    }
    different = []
    for generated, original_data, original_sentence in zip(normalized_input_sentences, real_dataset, original_sentences):
        action = original_data['action']
        if generated in sentences_per_action[action]:
            sentences_per_action[action][generated] += 1
        else:
            sentences_per_action[action][generated] = 1
        if generated != original_sentence:
            different.append({
                "action": action,
                "generated": generated,
                "original": original_sentence
            })
    print('SENTENCES PER ACTION:')
    for dc in DIRECTION_CODES:
        print(' ACTION {} ({} sentences)'.format(DIRECTION_CODES[dc], len(sentences_per_action[dc])))
        for s, nb in sentences_per_action[dc].items():
            print('  {}: {}'.format(nb, get_sentence_string(s, voc, text_normalizer)))
    print("DIFFERENCES")
    for d in different:
        print("ACTION {}:".format(DIRECTION_CODES[d['action']]))
        print("original:  ", get_sentence_string(d['original'],voc,text_normalizer))
        print("predicted: ", get_sentence_string(d['generated'], voc, text_normalizer))

if __name__ == "__main__":
    main()
