import argparse
import os
# USE with https://github.com/Maluuba/nlg-eval
from nlgeval import compute_metrics

parser = argparse.ArgumentParser()
parser.add_argument('--input-folder')
parser.add_argument('--root-folder')


def generate_stats(folder):
    print('processing: ', folder)
    generated_file = os.path.join(folder, 'generated')
    original_file = os.path.join(folder, 'original')
    metrics_dict = compute_metrics(hypothesis=generated_file,
                                   references=[original_file],
                                   no_skipthoughts=True,
                                   no_glove=True)
    return metrics_dict


def main():
    args = parser.parse_args()
    if args.root_folder is not None:
        models = ['output-best-validation-loss', 'output-best-training-loss']
        datasets = ['training', 'validation', 'test']
        methods = ['beam', 'greedy']
        all_stats = []
        for model in models:
            for d in datasets:
                for method in methods:
                    all_stats.append(
                        generate_stats(os.path.join(args.root_folder, model, '{}_{}_normalized'.format(d, method))))
    else:
        all_stats = [generate_stats(args.input_folder)]
    KEYS = ['Bleu_1', 'Bleu_2', 'Bleu_3', 'Bleu_4', 'METEOR', 'ROUGE_L', 'CIDEr']
    KEY_REPR = {
        'Bleu_1': 'BLEU-1',
        'Bleu_2': 'BLEU-2',
        'Bleu_3': 'BLEU-3',
        'Bleu_4': 'BLEU-4',
        'METEOR': 'METEOR',
        'ROUGE_L': 'ROUGE-L',
        'CIDEr': 'CIDEr'
    }
    for k in KEYS:
        output = '{} (\\%) '.format(KEY_REPR[k])
        for stats in all_stats:
            output += '& {} '.format(int(100 * stats[k]))
        output += '\\\\'
        print(output)


if __name__ == "__main__":
    main()
