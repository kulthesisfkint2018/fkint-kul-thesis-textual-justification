import argparse

from experiments.text_generation.keras_experiments.helpers import run_with_settings

ARCHITECTURE = 'merge'  # 'merge' or 'pre-inject'
INPUT_DIR = 'data/exported-labels/200-train-validation/word-last-fc/plain'
OUTPUT_DIR = 'data/text_generation_models/200-train-validation/keras_merge_ignore_observation'
DEMONSTRATIONS = 10
EPOCHS_PER_SESSION = 100
CHAR_LEVEL = False

parser = argparse.ArgumentParser()
parser.add_argument('--load_model', default=None)
parser.add_argument('--output_dir', default=OUTPUT_DIR)


def main():
    args = parser.parse_args()
    EXTRA_SETTINGS = {'vocabulary_size': 350, 'embedding_size': 128, 'ignore_observation': True}
    run_with_settings(ARCHITECTURE, INPUT_DIR, args.output_dir, DEMONSTRATIONS, EPOCHS_PER_SESSION, CHAR_LEVEL,
                      EXTRA_SETTINGS, args.load_model)


if __name__ == "__main__":
    main()
