import argparse
import time
from keras.models import load_model

import event_data
from experiments.agents.common import make_environment, play_atari
from experiments.text_generation.keras_experiments.helpers import generate_text_argmax, generate_text_beamsearch
from helpers import DEFAULT_MODEL_CONFIGURATION
from helpers import model_wrapper
from helpers.game_constants import DIRECTION_CODES
from lib.im2txt.vocabulary import Vocabulary
from text_generation.vocabulary_builder import TextNormalizer, TextCharNormalizer
from helpers.model_wrapper import get_player

CHAR_LEVEL = False

parser = argparse.ArgumentParser()
parser.add_argument('--load_model', required=True)
parser.add_argument('--vocabulary', required=True)
parser.add_argument('--max_length', default=None)
parser.add_argument('--char', action='store_true')


def main():
    args = parser.parse_args()
    voc = Vocabulary(args.vocabulary)
    max_length = args.max_length
    if args.char:
        text_normalizer = TextCharNormalizer()
        if max_length is None:
            max_length = 250
    else:
        text_normalizer = TextNormalizer()
        if max_length is None:
            max_length = 50

    model = load_model(args.load_model)
    agent = model_wrapper.CNNExtendedAgent(DEFAULT_MODEL_CONFIGURATION)

    event_filter = event_data.NewActionEventFilter(
        event_data.ProbabilisticEventFilter(.1, event_data.NonZeroPointsEventFilter()))

    def justify_action(processed_obs, action, total_reward):
        if event_filter.process_data(processed_obs, action, total_reward):
            beamsearch_sentence = generate_text_beamsearch(model, action, processed_obs, voc, text_normalizer,
                                                           max_length)
            argmax_sentence = generate_text_argmax(model, action, processed_obs, voc, text_normalizer, max_length)
            print('ACTION:     ', DIRECTION_CODES[action])
            print('beam search:', beamsearch_sentence)
            print('argmax:     ', argmax_sentence)
            input('Press enter to continue...')

    def play_and_justify():
        env = get_player(train=False)
        ob = env.reset()
        event_filter.reset()
        is_over = False
        total_reward = 0
        while not is_over:
            time.sleep(0.05)
            act, processed_observation = agent.predict(ob)
            justify_action(processed_observation, act, total_reward)
            ob, r, is_over, info = env.step(act)
            total_reward += r
            env.render()
        return None

    play_and_justify()


if __name__ == "__main__":
    main()
