import argparse

from experiments.text_generation.keras_experiments.helpers import run_with_settings

INPUT_DIR = 'data/exported-labels/200-train-validation/word-last-fc/plain'

parser = argparse.ArgumentParser()
parser.add_argument('--load_model', required=True)
parser.add_argument('--char', action='store_true')
parser.add_argument('--input_dir', default=None)
parser.add_argument('--vocabulary_path', default=None)


def main():
    args = parser.parse_args()
    EXTRA_SETTINGS = {}
    run_with_settings('', INPUT_DIR, '', 0, 0, args.char, EXTRA_SETTINGS, args.load_model,
                      vocabulary_path=args.vocabulary_path, validation_path=args.input_dir)


if __name__ == "__main__":
    main()
