import argparse
import os

from experiments.text_generation.dataset.build_actual_dataset import read_original_dataset
from text_generation.vocabulary_builder import TextNormalizer, TextCharNormalizer

parser = argparse.ArgumentParser()
parser.add_argument('--input_dir', required=True)
parser.add_argument('--dataset', choices=['training', 'test', 'validation'], required=True)
parser.add_argument('--method', choices=['beam', 'greedy'], required=True)
parser.add_argument('--original-data', required=True)
parser.add_argument('--char', action='store_true')


def write_normalized_sentences(sentences, path):
    with open(path, 'w') as f:
        f.writelines("\n".join(sentences))


def main():
    args = parser.parse_args()
    if args.char:
        text_normalizer = TextCharNormalizer()
    else:
        text_normalizer = TextNormalizer()
    generated_sentences_filename = os.path.join(args.input_dir, '{}_{}'.format(args.dataset, args.method))
    with open(generated_sentences_filename, 'r') as f:
        generated_sentences = f.readlines()
    generated_sentences = [text_normalizer.concatenate_normalized_words(text_normalizer.normalize_sentence(s)[1:-1]) for
                           s in generated_sentences]

    original_dataset = read_original_dataset(args.original_data)
    original_sentences = [x["label"] for x in original_dataset]
    original_sentences = [text_normalizer.concatenate_normalized_words(text_normalizer.normalize_sentence(s)) for s in
                          original_sentences]

    output_dir = os.path.join(args.input_dir, '{}_{}_normalized'.format(args.dataset, args.method))
    os.mkdir(output_dir)
    write_normalized_sentences(generated_sentences, os.path.join(output_dir, 'generated'))
    write_normalized_sentences(original_sentences, os.path.join(output_dir, 'original'))


if __name__ == "__main__":
    main()
