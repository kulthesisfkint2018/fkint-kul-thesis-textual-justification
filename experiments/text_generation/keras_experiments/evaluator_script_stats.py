import argparse
import os
from experiments.text_generation.dataset.build_actual_dataset import read_original_dataset

from text_generation.vocabulary_builder import TextNormalizer, TextCharNormalizer
parser = argparse.ArgumentParser()
parser.add_argument('--root-folder', required=True)
parser.add_argument('--training_data', required=True)
parser.add_argument('--char', action='store_true')


def generate_stats(folder, original_sentences):
    print('processing: ', folder)
    with open(os.path.join(folder, 'generated'), 'r') as f:
        generated_sentences = list(map(lambda x: x.strip(), f.readlines()))
    generated_sentences_not_in_original = [s for s in generated_sentences if s not in original_sentences]
    metrics_dict = {
        'unique': len(set(generated_sentences)),
        'new': len(generated_sentences_not_in_original),
        'new_unique': len(set(generated_sentences_not_in_original)),
    }
    return metrics_dict


def main():
    args = parser.parse_args()

    if args.char:
        text_normalizer = TextCharNormalizer()
    else:
        text_normalizer = TextNormalizer()

    dataset = read_original_dataset(args.training_data)

    original_sentences = [l['label'] for l in dataset]
    original_sentences = [text_normalizer.concatenate_normalized_words(text_normalizer.normalize_sentence(s)) for s in
                          original_sentences]

    if args.root_folder is not None:
        models = ['output-best-validation-loss', 'output-best-training-loss']
        datasets = ['training', 'validation', 'test']
        methods = ['beam', 'greedy']
        all_stats = []
        for model in models:
            for d in datasets:
                for method in methods:
                    all_stats.append(
                        generate_stats(os.path.join(args.root_folder, model, '{}_{}_normalized'.format(d, method)), original_sentences))
    else:
        all_stats = [generate_stats(args.input_folder)]
    KEYS = ['unique', 'new', 'new_unique']
    KEY_REPR = {
        'unique': 'Unique',
        'new': 'New (with dupl.)',
        'new_unique': 'Unique new'
    }
    for k in KEYS:
        output = '{} '.format(KEY_REPR[k])
        for stats in all_stats:
            output += '& {} '.format(stats[k])
        output += '\\\\'
        print(output)


if __name__ == "__main__":
    main()
