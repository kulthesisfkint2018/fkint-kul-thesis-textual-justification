import keras.backend as K
from keras.callbacks import ModelCheckpoint
from keras.layers import Dense
from keras.layers import Embedding, Concatenate, TimeDistributed, RepeatVector
from keras.layers import Input, Lambda
from keras.layers import LSTM, Dropout
from keras.layers import Masking
from keras.models import Model, load_model
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical, plot_model
from numpy import array

from experiments.text_generation.dataset.helpers import *
from helpers.game_constants import DIRECTION_CODES
from lib.im2txt.vocabulary import Vocabulary
from text_generation.beam_search import BeamSearch, Sentence, WordProbability
from text_generation.inference import get_sentence_string
from text_generation.inputs import get_vocabulary_path
from text_generation.vocabulary_builder import TextNormalizer, TextCharNormalizer

SHAPE_ACTION = (1,)


def get_params(vocabulary, optimizer="adam",
               lstm_size=512,
               lstm_dropout_keep_prob=.7,
               vocabulary_size=1200,
               embedding_size=512,
               ignore_observation=False,
               ignore_action=False,
               max_length=30,
               processed_observation_shape=(512,),
               normalize_observation=False,
               merge_output_features_size=64,
               merge_game_features_size=64,
               input_dropout_keep_prob=1
               ):
    return {
        "optimizer": optimizer,
        "lstm_size": lstm_size,
        "lstm_dropout_prob": 1 - lstm_dropout_keep_prob,
        "vocabulary_size": vocabulary_size,
        "embedding_size": embedding_size,
        "ignore_observation": ignore_observation,
        "ignore_action": ignore_action,
        "vocabulary": vocabulary,
        "max_length": max_length,
        "processed_observation_shape": processed_observation_shape,
        "normalize_observation": normalize_observation,
        "merge_game_features_size": merge_game_features_size,
        "merge_output_features_size": merge_output_features_size,
        "input_dropout_prob": 1 - input_dropout_keep_prob
    }


def define_merge_model(params):
    inputs, game_input, sequence_embedded = get_model_inputs(params)

    game_features_size = params['merge_game_features_size']
    fe2 = Dense(game_features_size, activation='relu')(game_input)

    se2 = LSTM(params['lstm_size'])(sequence_embedded)
    se2 = Dropout(rate=params['lstm_dropout_prob'])(se2)
    decoder1 = Concatenate(axis=1)([fe2, se2])
    output_features_size = params['merge_output_features_size']
    decoder2 = Dense(output_features_size, activation='relu')(decoder1)
    return get_model_with_output(decoder2, inputs, params)


def define_preinject_model(params):
    inputs, game_input, sequence_embedded = get_model_inputs(params, mask_zero=False)

    fe2 = Dense(params['embedding_size'], activation='relu')(game_input)
    game_embedding = RepeatVector(1)(fe2)
    merged = Concatenate(axis=1)([game_embedding, sequence_embedded])

    merged_shape = [params['max_length'] + 1, params['embedding_size']]
    masked_merged = Lambda(build_mask_applier(params, pre_inject=True, tensor_size=params['embedding_size']),
                           output_shape=merged_shape)([inputs[2], merged])
    masked_merged = Masking()(masked_merged)

    word_predictions = LSTM(params['lstm_size'], return_sequences=False)(masked_merged)
    word_predictions = Dropout(rate=params['lstm_dropout_prob'])(word_predictions)

    return get_model_with_output(word_predictions, inputs, params)


def define_imgcap_model(params):
    """
    https://github.com/1heart/image-caption/blob/master/imgcap_model.ipynb
    :param vocab_size:
    :param max_length:
    :param params:
    :return:
    """
    # state concatenated for every sequence step
    # no masking for post-sequence => model needs to learn to output 0 too
    # 1 RNN for sequence model and 1 after combining with image data
    inputs, game_input, sequence_embedded = get_model_inputs(params)

    game_input_size = 256
    fe2 = Dense(game_input_size, activation='relu')(game_input)
    repeated_game_input = RepeatVector(params['max_length'])(fe2)

    # merge for 1st LSTM
    sequence_model = LSTM(params['lstm_size'], return_sequences=True)(sequence_embedded)
    sequence_prediction_size = 128
    sequence_predictions = TimeDistributed(Dense(sequence_prediction_size))(sequence_model)

    # par-inject for 2nd LSTM
    merged = Concatenate(axis=2)([repeated_game_input, sequence_predictions])
    merged_shape = [params['max_length'], game_input_size + sequence_prediction_size]
    masked_merged = Lambda(
        build_mask_applier(params, tensor_size=game_input_size + sequence_prediction_size, pre_inject=False),
        output_shape=merged_shape)(
        [sequence_embedded, merged])
    masked_merged = Masking()(masked_merged)

    word_predictions = LSTM(params['lstm_size'], return_sequences=False)(masked_merged)
    return get_model_with_output(word_predictions, inputs, params)


MODEL_FUNCTIONS = {
    'merge': define_merge_model,
    'pre-inject': define_preinject_model,
}


def define_model(architecture, params):
    return MODEL_FUNCTIONS[architecture](params)


def get_model_with_output(word_predictions, inputs, params):
    outputs = Dense(params['vocabulary_size'], activation='softmax')(word_predictions)
    model = Model(inputs=inputs, outputs=outputs)
    model.compile(loss='categorical_crossentropy', optimizer=params['optimizer'])
    return model


def get_model_inputs(params, mask_zero=True):
    action_inputs = Input(shape=SHAPE_ACTION)  # action
    processed_observation_inputs = Input(shape=params['processed_observation_shape'])  # processed observation
    dropout_observation = processed_observation_inputs
    dropout_observation = process_observation(dropout_observation, params)
    game_input = Concatenate(axis=1)([action_onehot(action_inputs), dropout_observation])

    inputs2 = Input(shape=(params['max_length'],))  # sequence
    se1 = Embedding(params['vocabulary_size'] + 1, params['embedding_size'], mask_zero=mask_zero)(inputs2)

    return (action_inputs, processed_observation_inputs, inputs2), game_input, se1


def action_onehot(action):
    return Lambda(lambda x: (K.reshape(K.one_hot(K.tf.to_int32(x), num_classes=9),
                                       (-1, 9))),
                  output_shape=(9,))(action)


def normalize_observation(observation, params):
    return Lambda(lambda x: K.tf.divide(x, K.tf.reshape(K.tf.norm(x, axis=1), (-1, 1))),
                  output_shape=params['processed_observation_shape'])(observation)


def zeros_like_observation(observation, params):
    return Lambda(lambda x: K.tf.zeros_like(x), output_shape=params['processed_observation_shape'])(observation)


def process_observation(observation, params):
    if params['normalize_observation']:
        observation = normalize_observation(observation, params)
    if params['ignore_observation']:
        observation = zeros_like_observation(observation, params)
    return observation


def build_mask_applier(params, tensor_size, pre_inject=False):
    def apply_mask(tensors):
        sequence_tensor = tensors[0]
        merged_tensor = tensors[1]
        if pre_inject:
            extended_tensor = K.concatenate([
                K.ones(K.concatenate([K.shape(sequence_tensor)[:1], (1,)])),
                sequence_tensor
            ], axis=1)
        else:
            extended_tensor = sequence_tensor

        all_zeros = K.zeros_like(extended_tensor)
        is_nonzero = K.not_equal(extended_tensor, all_zeros)
        expanded_all_zeros = K.expand_dims(all_zeros, axis=2)
        expanded_all_zeros = K.repeat_elements(expanded_all_zeros, rep=tensor_size, axis=2)
        expanded_is_nonzero = K.expand_dims(is_nonzero, axis=2)
        expanded_is_nonzero = K.repeat_elements(expanded_is_nonzero, rep=tensor_size, axis=2)
        masked = K.tf.where(expanded_is_nonzero, merged_tensor, expanded_all_zeros)
        return masked

    return apply_mask


def debug_tensor(t):
    return K.tf.Print(t, [t], 'tf-debug', first_n=1000, summarize=1000)


def load_data_record(directory):
    with open(os.path.join(directory, LABEL_ACTION_FILENAME), 'r') as f:
        label_and_action = json.load(f)
    return {
        "action": label_and_action['action'],
        "label": label_and_action['label'],
        "processed_observation": np.load(os.path.join(directory, PROCESSED_OBSERVATION_FILENAME))
    }


def load_data(directory):
    with open(os.path.join(directory, INDEX_FILENAME), 'r') as f:
        index = json.load(f)
    return [load_data_record(os.path.join(directory, p)) for p in index['paths']]


# calculate the length of the description with the most words
def get_max_length_of_labels(labels):
    return max(len(l['label']) for l in labels)


def create_pacman_sequences(labels, params):
    actions = []
    processed_observations = []
    captions_in = []
    captions_out = []
    for label in labels:
        for i in range(1, len(label['label'])):
            in_seq, out_seq = label['label'][:i], label['label'][i]
            in_seq = pad_sequences([in_seq], maxlen=params['max_length'])[0]
            out_seq = to_categorical([out_seq], num_classes=params['vocabulary_size'])[0]

            actions.append(label['action'])
            processed_observations.append(label['processed_observation'])
            captions_in.append(in_seq)
            captions_out.append(out_seq)
    return array(actions, dtype=np.int), array(processed_observations), array(captions_in), array(captions_out)


def generate_text_argmax(model, action, observation, voc, text_normalizer, max_length):
    in_seq = [voc.start_id]
    for i in range(max_length):
        sequence = pad_sequences([in_seq], maxlen=max_length)
        next_word_probs = model.predict([array([action]), array([observation]), sequence], verbose=0)
        next_word = np.argmax(next_word_probs)
        in_seq.append(next_word)
        if next_word == voc.end_id:
            break
    return get_sentence_string(in_seq, voc, text_normalizer)


def generate_text_beamsearch(model, action, observation, voc, text_normalizer, max_length):
    nb_candidates = 10
    best_complete_sentences = BeamSearch(nb_candidates)
    best_partial_sentences = BeamSearch(nb_candidates)

    best_partial_sentences.add_sentence(
        Sentence([WordProbability(voc.start_id, 1.)], None, length_normalization_factor=1)
    )

    for _ in range(max_length):
        if best_partial_sentences.is_empty():
            break
        current_partial_sentences = best_partial_sentences.extract()
        sequences = pad_sequences([partial_sentence.get_word_ids() for partial_sentence in current_partial_sentences],
                                  maxlen=max_length)

        next_word_probs = model.predict([array([action for _ in current_partial_sentences]),
                                         array([observation for _ in current_partial_sentences]), sequences], verbose=0)

        for old_sentence, word_probs in zip(current_partial_sentences, next_word_probs):
            for word_id in map(int, np.argsort(word_probs)[-5:]):
                sentence = old_sentence.extend(word_id, word_probs[word_id], None)
                if sentence.score() < 1. / nb_candidates:
                    continue
                # print('new ids: ', sentence.get_word_ids())
                # print('old sentence: ', get_sentence_string(old_sentence.get_word_ids(), voc, text_normalizer),
                #       'new sentence:',
                #       get_sentence_string(sentence.get_word_ids(), voc, text_normalizer))
                if sentence.is_complete(voc):
                    best_complete_sentences.add_sentence(sentence)
                else:
                    best_partial_sentences.add_sentence(sentence)

    if best_complete_sentences.is_empty():
        # print('best complete sentences is empty!')
        return None
    best_sentence = best_complete_sentences.get_highest_n(1)[0]
    best_sentences = best_complete_sentences.extract()

    # print('complete sentences: \n{}'.format(
    #     "\n".join("{}: {}".format(score, sentence) for score, sentence in sorted(
    #         [(s.score(), get_sentence_string(s.get_word_ids(), voc, text_normalizer=text_normalizer)) for s in
    #          best_sentences]))))
    return get_sentence_string(best_sentence.get_word_ids(), voc, text_normalizer=text_normalizer)


def demonstrate_performance(model, labels, max_length, voc, text_normalizer):
    actual, predicted_argmax, predicted_beamsearch = [], [], []
    predictions_per_action_argmax = {x: set() for x in DIRECTION_CODES.keys()}
    predictions_per_action_beamsearch = {x: set() for x in DIRECTION_CODES.keys()}
    for label in labels:
        action = label['action']
        processed_observation = label['processed_observation']

        predicted_label_argmax = generate_text_argmax(model, action, processed_observation, voc, text_normalizer,
                                                      max_length)
        predicted_argmax.append(predicted_label_argmax)
        predictions_per_action_argmax[action].add(predicted_label_argmax)

        predicted_label_beamsearch = generate_text_beamsearch(model, action, processed_observation, voc,
                                                              text_normalizer, max_length)
        predicted_beamsearch.append(predicted_label_beamsearch)
        predictions_per_action_beamsearch[action].add(predicted_label_beamsearch)

        actual_label = get_sentence_string(label['label'], voc, text_normalizer)
        actual.append(actual_label)
        print('action:', DIRECTION_CODES[action], 'actual:', actual_label)
        print('predicted (argmax):      ', predicted_label_argmax)
        print('predicted (beam search): ', predicted_label_beamsearch)
    print('ALL ARGMAX:')
    print("\n".join(predicted_argmax))
    print("ARGMAX results per action")
    for d in DIRECTION_CODES.keys():
        print(DIRECTION_CODES[d], ":", predictions_per_action_argmax[d])
    print('ALL BEAMSEARCH:')
    print("\n".join(predicted_beamsearch))
    print("BEAMSEARCH results per action")
    for d in DIRECTION_CODES.keys():
        print(DIRECTION_CODES[d], ":", predictions_per_action_beamsearch[d])


def train_model(model, training_labels, evaluation_labels, params, output_dir, nb_epochs=10):
    train_actions, train_processed_observation, train_captions_input, train_captions_output = \
        create_pacman_sequences(training_labels, params)

    test_actions, test_processed_observation, test_captions_input, test_captions_output = \
        create_pacman_sequences(evaluation_labels, params)

    os.makedirs(output_dir, exist_ok=False)
    filepath = os.path.join(output_dir, 'model-ep.{epoch:03d}-loss.{loss:.3f}-val_loss.{val_loss:.3f}.h5')

    model.summary()
    plot_model(model, to_file='model.png', show_shapes=True)

    checkpoint = ModelCheckpoint(filepath, monitor='val_loss', verbose=1, save_best_only=False, mode='min')
    # fit model
    model.fit([train_actions, train_processed_observation, train_captions_input], train_captions_output,
              epochs=nb_epochs,
              verbose=2,
              callbacks=[checkpoint],
              validation_data=([test_actions, test_processed_observation, test_captions_input], test_captions_output))


def run_with_settings(architecture, input_dir, output_dir,
                      demonstrations,
                      epochs_per_session,
                      char_level, param_settings, model_path=None, vocabulary_path=None, validation_path=None):
    if vocabulary_path is None:
        vocabulary_path = get_vocabulary_path(input_dir)
    voc = Vocabulary(vocabulary_path)
    if char_level:
        text_normalizer = TextCharNormalizer()
        param_settings['max_length'] = 250
    else:
        text_normalizer = TextNormalizer()
        param_settings['max_length'] = 50

    params = get_params(voc, **param_settings)

    if model_path is not None:
        model = load_model(model_path)
    else:
        model = define_model(architecture, params)


    validation_labels = load_data(validation_path if validation_path is not None else os.path.join(input_dir, VALIDATION_FOLDERNAME))

    if demonstrations == 0:
        demonstrate_performance(model, validation_labels, params['max_length'], voc, text_normalizer)
    else:
        training_labels = load_data(os.path.join(input_dir, TRAIN_FOLDERNAME))
        validation_labels = load_data(os.path.join(input_dir, VALIDATION_FOLDERNAME))
        os.mkdir(output_dir)
        for i in range(demonstrations):
            print('DEMO', i, '/', demonstrations)
            train_model(model, training_labels, validation_labels, params, os.path.join(output_dir, "{:03d}".format(i)),
                        epochs_per_session)
            demonstrate_performance(model, validation_labels, params['max_length'], voc, text_normalizer)
