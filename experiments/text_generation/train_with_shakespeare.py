import argparse
import logging
import os

import tensorflow as tf

from helpers.game_constants import DIRECTION_CODES
from lib.im2txt.vocabulary import Vocabulary
from text_generation.inference import sentence_inference_with_beam_search
from text_generation.inputs import get_vocabulary_path, make_fake_sentences_training_input
from text_generation.models.model import TextGenerationModel
from text_generation.vocabulary_builder import VocabularyBuilder, TextNormalizer, TextCharNormalizer


def build_vocabulary(input_sentences_path, vocabulary_path, output_sentences_path, text_normalizer):
    with open(input_sentences_path, 'r') as f:
        sentences = [l.strip() for l in f.readlines()]
        sentences = [s for s in sentences if len(s) > 0]

    vocabulary_builder = VocabularyBuilder()
    with open(output_sentences_path, 'w') as f:
        for sentence in sentences:
            normalized_sentence = text_normalizer.normalize_sentence(sentence)
            if len(normalized_sentence) == 0:
                continue
            vocabulary_builder.feed_sentence(normalized_sentence)
            f.write('{}\n'.format(text_normalizer.output_word_separator().join(normalized_sentence)))

    with open(vocabulary_path, 'w') as f:
        f.writelines(x + "\n" for x in vocabulary_builder.build_vocabulary_list(max_vocab_size=999999))


def main():
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
    parser = argparse.ArgumentParser()
    parser.add_argument('--steps', help='number of training steps', default=1000, type=int)
    parser.add_argument('--sentences_limit', help='maximal number of sentences', default=-1, type=int)
    parser.add_argument('--traindir', help='directory with training directory and all-sentences file',
                        default='./data/exported-labels/fake-sentences-brugge/')
    parser.add_argument('--predictdir', help='directory with data.tfrecord to run the trained model on',
                        default='./data/exported-labels/action-change-after-start-200-processed-split')
    parser.add_argument('--modeldir', help='directory to store model checkpoints', default=None)
    parser.add_argument('--batch_size', help='batch size', default=30, type=int)
    parser.add_argument('--build_voc_input_file', help='from which input file the vocabulary should be built',
                        default=None)
    parser.add_argument('--learning_rate_decay_factor', type=float, help='decay factor for learning rate', default=0)
    parser.add_argument('--optimizer', default="SGD")
    parser.add_argument('--learning_rate', type=float, default=0.01)
    parser.add_argument('--lstm_size', type=int, default=256)
    parser.add_argument('--nb_lstms', type=int, default=1)
    parser.add_argument('--char_level', type=bool, default=False)
    parser.add_argument('--embedding_size', type=int, default=128)
    parser.add_argument('--ignore_observation', type=bool, default=False)
    parser.add_argument('--ignore_action', type=bool, default=False)
    parser.add_argument('--pass_state_every_prediction', type=bool, default=False)
    parser.add_argument('--l2_regularization_factor', type=float, default=1.0e-6)
    args = parser.parse_args()

    TRAINING_DATA_DIRECTORY = args.traindir
    sentences_path = os.path.join(TRAINING_DATA_DIRECTORY, 'all-sentences')
    vocabulary_path = get_vocabulary_path(TRAINING_DATA_DIRECTORY)
    filenames = [sentences_path]
    if args.char_level:
        text_normalizer = TextCharNormalizer()
    else:
        text_normalizer = TextNormalizer()
    if args.build_voc_input_file is not None:
        print('start building vocabulary')
        build_vocabulary(args.build_voc_input_file, vocabulary_path, sentences_path, text_normalizer=text_normalizer)
        print('finished building vocabulary')
        return

    voc = Vocabulary(vocabulary_path)
    print('vocabulary size: ', voc.get_vocabulary_size())
    nb_sentences = len(open(sentences_path, 'r').readlines())
    print('# sentences: ', nb_sentences)
    params = {
        "optimizer": args.optimizer,
        "clip_gradients": 5.0,
        "learning_rate": args.learning_rate,
        "initializer_scale": 0.08,
        "lstm_size": args.lstm_size,
        "lstm_dropout_keep_prob": .7,
        "lstm_configuration": [
            {
                "lstm_size": args.lstm_size,
                "lstm_dropout_keep_prob": .7
            } for _ in range(args.nb_lstms)
        ],
        "observation_size": 512,
        "vocabulary_size": voc.get_vocabulary_size(),
        "embedding_size": args.embedding_size,
        "pass_state_on_every_lstm_iteration": args.pass_state_every_prediction,
        "vocabulary": voc,
        "ignore_observation": args.ignore_observation,
        "ignore_action": args.ignore_action,
        "num_examples_per_epoch": nb_sentences,
        "batch_size": args.batch_size,
        "num_epochs_per_decay": 5,
        "learning_rate_decay_factor": args.learning_rate_decay_factor,
        "l2_regularization_factor": args.l2_regularization_factor
    }

    text_estimator = tf.estimator.Estimator(model_fn=TextGenerationModel.get_model_fn(),
                                            params=params,
                                            model_dir=args.modeldir)
    tf.logging.set_verbosity(tf.logging.INFO)
    text_estimator.train(
        input_fn=make_fake_sentences_training_input(filenames, voc, text_normalizer, params['batch_size'],
                                                    limit=args.sentences_limit,
                                                    repeats=-1),
        steps=args.steps)
    print('done training')
    tf.logging.set_verbosity(tf.logging.WARN)
    actions = []
    sentences = []
    fake_data_input_iterator = make_fake_sentences_training_input(filenames, voc, text_normalizer, 1, 10)()
    # for data in get_record_features(TEST_DATA_DIRECTORY):
    with tf.Session() as sess:
        try:
            while True:
                data = sess.run(fake_data_input_iterator)
                features = data[0]
                action = DIRECTION_CODES[int(features["selected_policy"][0])]
                actions.append(action)
                sentence = sentence_inference_with_beam_search(text_estimator, voc, text_normalizer, {
                    "selected_policy": features['selected_policy'][0],
                    "processed_observation": features["processed_observation"][0]
                }, params)
                sentences.append(sentence)
                print("Got {}: {}".format(action, sentence))
        except Exception as ex:
            if isinstance(ex, tf.errors.OutOfRangeError):
                print('Done: ', type(ex))
            else:
                print('Error: ', ex)
                logging.exception('message')
    print('all sentences: ', set(sentences))
    sentences_per_action = {action: [sentence for a, sentence in zip(actions, sentences) if a == action] for action in
                            DIRECTION_CODES.values()}
    print('sentences per action: ', "\n".join("{}: {}".format(key, val) for key, val in sentences_per_action.items()))


if __name__ == "__main__":
    main()
