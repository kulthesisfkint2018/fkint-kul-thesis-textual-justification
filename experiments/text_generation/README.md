# Dataset
See `dataset`.

* `build_train_test_split`: script to extract labelled data from the labelling interface's database.
* `build_actual_dataset`: script to build a training and validation database using the extracted labelled data (configuration options for word/character-level tokenization and processed observation data).
* `analysis`: script to analyze the dataset.

# Show and Tell-based pre-inject model
See `im2txt_experiments`.

* `im2txt_parameters`: default settings of Show and Tell model using word-level tokenization.
* `im2txt_char`: default settings of Show and Tell model using character-level tokenization.
* `im2txt_adam`: Show and Tell model using Adam optimizer.
* `im2txt_adam_ignore_observation`: Show and Tell model using Adam optimizer, ignoring the processed observation (state data only consists of the action).
* `im2txt_adam_ignore_observation_small_dataset`, `im2txt_adam_ignore_observation_small_dataset_small_learning_rate`, `im2txt_adam_smaller_embedding`: other experiments with smaller dataset and smaller embedding size.


# Keras-based implementations
See `keras_experiments`.

* `preinject_initial`: Keras implementation of Show and Tell architecture.
* `merge_initial`: Keras implementation of merge architecture using same parameters as Show and Tell architecture.
* `merge_initial_ignore_observation`: Merge architecture but ignoring the processed observation (the state data only consists of the selected action).
* `merge_lstm_512_normalize`: Merge architecture but normalizing the processed observation.
* `merge_lstm_512_dropout`: Merge architecture with dropout regularization added to the processed observation.
* `merge_lstm_64`, `merge_lstm_128`, `merge_lstm_256`: Experiments with different numbers of LSTM units.
* `merge_lstm_128_embedding_64`, `merge_lstm_128_embedding_256`: Experiments with different embedding sizes.
* `merge_final`: Final merge architecture used for evaluation.
* `analyze_sentences`: Helper script to analyze generated sentences on the validation dataset.
* `evaluate_existing`: Helper script to evaluate a trained Keras model.
* `run_with_game`: Script generating captions while the agent plays (using the pipeline components in this repository).

# Other
* `train_with_shakespeare`: separate experiment with shakespeare dataset (not discussed in thesis)
