"""
Code based on https://github.com/awjuliani/DeepRL-Agents
Alternative AC network based on https://github.com/dgriff777/rl_a3c_pytorch/blob/master/model.py
"""
import argparse
import logging
import multiprocessing
import os
import threading
import time

import numpy as np
import scipy.signal
import tensorflow as tf
import tensorflow.contrib.slim as slim

from experiments.agents.common import play_atari, evaluate_atari
from helpers.graphical import make_gif

parser = argparse.ArgumentParser()
parser.add_argument('--env', type=str, default='MsPacmanNoFrameskip-v0')
parser.add_argument('--model_path', type=str, default='./data/agent_models/plain_a3c_model_checkpoints/')
parser.add_argument('--gifs_path', type=str, default='/tmp/plain_a3c_model_gifs/')
parser.add_argument('--load_model', type=int, default=0)
parser.add_argument('--mode', choices=['eval', 'play', 'train'], default='train')
parser.add_argument('--nb_evaluations', default=100, type=int)
parser.add_argument('--evaluation_output_file', default='evaluation.csv', type=str)

args = parser.parse_args()
GLOBAL_SCOPE = 'global'
GYM_GAME_NAME = args.env
IMAGE_WIDTH = 210
IMAGE_HEIGHT = 160
IMAGE_COLOUR_DEPTH = 3

OBSERVATION_DIMENSIONS = (84, 84, 1)
# STATE_DIMENSIONS = IMAGE_WIDTH * IMAGE_HEIGHT * IMAGE_COLOUR_DEPTH
NB_ACTIONS = 9
GAMMA = .99
MAX_EPISODE_BUFFER_LENGTH = 100
MAX_EPISODE_LENGTH = 300
MODEL_PATH = args.model_path
GIFS_PATH = args.gifs_path
LOAD_MODEL = bool(args.load_model)

logging.basicConfig(level=logging.DEBUG)

if not os.path.exists(MODEL_PATH):
    os.makedirs(MODEL_PATH)
if not os.path.exists(GIFS_PATH):
    os.makedirs(GIFS_PATH)


def update_target_graph(src_scope, dest_scope):
    src_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, src_scope)
    dst_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, dest_scope)

    return [dst_var.assign(src_var) for src_var, dst_var in zip(src_vars, dst_vars)]


def normalized_columns_initializer(std=1.0):
    def _initializer(shape, dtype=None, partition_info=None):
        out = np.random.randn(*shape).astype(np.float32)
        out *= std / np.sqrt(np.square(out).sum(axis=0, keepdims=True))
        return tf.constant(out)

    return _initializer


def discount(x, gamma):
    return scipy.signal.lfilter([1], [1, -gamma], x[::-1], axis=0)[::-1]


class ACNetworkDgriff777(object):
    # https://github.com/dgriff777/rl_a3c_pytorch/blob/master/model.py
    def __init__(self, scope, trainer):
        with tf.variable_scope(scope):
            """
            torch.nn.Conv2d(in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, groups=1, bias=True)
            torch.nn.MaxPool2d(kernel_size, stride=None, padding=0, dilation=1, return_indices=False, ceil_mode=False)
            self.conv1 = nn.Conv2d(num_inputs, 32, 5, stride=1, padding=2)
            self.maxp1 = nn.MaxPool2d(2, 2)
            self.conv2 = nn.Conv2d(32, 32, 5, stride=1, padding=1)
            self.maxp2 = nn.MaxPool2d(2, 2)
            self.conv3 = nn.Conv2d(32, 64, 4, stride=1, padding=1)
            self.maxp3 = nn.MaxPool2d(2, 2)
            self.conv4 = nn.Conv2d(64, 64, 3, stride=1, padding=1)
            self.maxp4 = nn.MaxPool2d(2, 2)
            """
            # Visual
            self.inputs = tf.placeholder(shape=(None,) + OBSERVATION_DIMENSIONS, dtype=tf.float32)

            self.conv1 = slim.conv2d(activation_fn=tf.nn.elu, inputs=self.inputs, num_outputs=32,
                                     kernel_size=[5, 5], stride=[1, 1], padding='VALID')
            self.maxp1 = slim.max_pool2d(self.conv1, kernel_size=2, stride=2)
            self.conv2 = slim.conv2d(activation_fn=tf.nn.elu, inputs=self.maxp1, num_outputs=32, kernel_size=[5, 5],
                                     stride=[1, 1], padding='VALID')
            self.maxp2 = slim.max_pool2d(self.conv2, kernel_size=2, stride=2)
            self.conv3 = slim.conv2d(activation_fn=tf.nn.elu, inputs=self.maxp2, num_outputs=64, kernel_size=[4, 4],
                                     stride=[1, 1], padding='VALID')
            self.maxp3 = slim.max_pool2d(self.conv3, kernel_size=2, stride=2)
            self.conv4 = slim.conv2d(activation_fn=tf.nn.elu, inputs=self.maxp3, num_outputs=64, kernel_size=[3, 3],
                                     stride=[1, 1], padding='VALID')
            self.maxp4 = slim.max_pool2d(self.conv4, kernel_size=2, stride=4)

            hidden = slim.fully_connected(slim.flatten(self.maxp4), 512, activation_fn=tf.nn.elu)

            # Recurrent network
            lstm_cell = tf.contrib.rnn.BasicLSTMCell(1024, state_is_tuple=True)
            c_init = np.zeros((1, lstm_cell.state_size.c), np.float32)
            h_init = np.zeros((1, lstm_cell.state_size.h), np.float32)
            self.state_init = [c_init, h_init]
            c_in = tf.placeholder(tf.float32, [1, lstm_cell.state_size.c])
            h_in = tf.placeholder(tf.float32, [1, lstm_cell.state_size.h])
            self.state_in = (c_in, h_in)
            rnn_in = tf.expand_dims(hidden, [0])
            step_size = tf.shape(self.inputs)[:1]
            state_in = tf.contrib.rnn.LSTMStateTuple(c_in, h_in)
            lstm_outputs, lstm_state = tf.nn.dynamic_rnn(lstm_cell, rnn_in, initial_state=state_in,
                                                         sequence_length=step_size, time_major=False)
            logging.info('lstm outputs shape: %s', lstm_outputs.get_shape())
            lstm_c, lstm_h = lstm_state
            self.state_out = (lstm_c[:1, :], lstm_h[:1, :])
            rnn_out = tf.reshape(lstm_outputs, [-1, 1024])

            # Output for policy and value estimation
            self.policy = slim.fully_connected(rnn_out, NB_ACTIONS, activation_fn=tf.nn.softmax,
                                               weights_initializer=normalized_columns_initializer(0.01),
                                               biases_initializer=None)
            self.value = slim.fully_connected(rnn_out, 1, activation_fn=None,
                                              weights_initializer=normalized_columns_initializer(1.0),
                                              biases_initializer=None)

            if scope != GLOBAL_SCOPE:
                # update gradients
                self.actions = tf.placeholder(shape=[None], dtype=tf.int32)
                self.actions_onehot = tf.one_hot(self.actions, NB_ACTIONS, dtype=tf.float32)
                self.target_value = tf.placeholder(shape=[None], dtype=tf.float32)
                self.advantages = tf.placeholder(shape=[None], dtype=tf.float32)

                self.responsible_outputs = tf.reduce_sum(self.policy * self.actions_onehot, [1])

                # Loss functions
                self.value_loss = .5 * tf.reduce_sum(tf.square(self.target_value - tf.reshape(self.value, [-1])))
                self.entropy = -tf.reduce_sum(self.policy * tf.log(self.policy))
                self.policy_loss = -tf.reduce_sum(tf.log(self.responsible_outputs) * self.advantages)
                self.loss = .5 * self.value_loss + self.policy_loss - self.entropy * .01

                # Local network's gradients
                local_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope)
                self.gradients = tf.gradients(self.loss, local_vars)
                self.var_norms = tf.global_norm(local_vars)
                grads, self.grad_norms = tf.clip_by_global_norm(self.gradients, 40.0)

                # Apply local gradients to global network
                global_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, GLOBAL_SCOPE)
                self.apply_grads = trainer.apply_gradients(zip(grads, global_vars))


class ACNetworkAWJuliani(object):
    # https://github.com/awjuliani/DeepRL-Agents
    def __init__(self, scope, trainer):
        with tf.variable_scope(scope):
            # Visual
            self.inputs = tf.placeholder(shape=(None,) + OBSERVATION_DIMENSIONS, dtype=tf.float32)

            self.conv1 = slim.conv2d(activation_fn=tf.nn.elu, inputs=self.inputs, num_outputs=16,
                                     kernel_size=[8, 8], stride=[4, 4], padding='VALID')

            self.conv2 = slim.conv2d(activation_fn=tf.nn.elu, inputs=self.conv1, num_outputs=32, kernel_size=[4, 4],
                                     stride=[2, 2], padding='VALID')

            hidden = slim.fully_connected(slim.flatten(self.conv2), 256, activation_fn=tf.nn.elu)

            # Recurrent network
            lstm_cell = tf.contrib.rnn.BasicLSTMCell(256, state_is_tuple=True)
            c_init = np.zeros((1, lstm_cell.state_size.c), np.float32)
            h_init = np.zeros((1, lstm_cell.state_size.h), np.float32)
            self.state_init = [c_init, h_init]
            c_in = tf.placeholder(tf.float32, [1, lstm_cell.state_size.c])
            h_in = tf.placeholder(tf.float32, [1, lstm_cell.state_size.h])
            self.state_in = (c_in, h_in)
            rnn_in = tf.expand_dims(hidden, [0])
            step_size = tf.shape(self.inputs)[:1]
            state_in = tf.contrib.rnn.LSTMStateTuple(c_in, h_in)
            lstm_outputs, lstm_state = tf.nn.dynamic_rnn(lstm_cell, rnn_in, initial_state=state_in,
                                                         sequence_length=step_size, time_major=False)
            lstm_c, lstm_h = lstm_state
            self.state_out = (lstm_c[:1, :], lstm_h[:1, :])
            rnn_out = tf.reshape(lstm_outputs, [-1, 256])

            # Output for policy and value estimation
            self.policy = slim.fully_connected(rnn_out, NB_ACTIONS, activation_fn=tf.nn.softmax,
                                               weights_initializer=normalized_columns_initializer(0.01),
                                               biases_initializer=None)
            self.value = slim.fully_connected(rnn_out, 1, activation_fn=None,
                                              weights_initializer=normalized_columns_initializer(1.0),
                                              biases_initializer=None)

            if scope != GLOBAL_SCOPE:
                # update gradients
                self.actions = tf.placeholder(shape=[None], dtype=tf.int32)
                self.actions_onehot = tf.one_hot(self.actions, NB_ACTIONS, dtype=tf.float32)
                self.target_value = tf.placeholder(shape=[None], dtype=tf.float32)
                self.advantages = tf.placeholder(shape=[None], dtype=tf.float32)

                self.responsible_outputs = tf.reduce_sum(self.policy * self.actions_onehot, [1])

                # Loss functions
                self.value_loss = .5 * tf.reduce_sum(tf.square(self.target_value - tf.reshape(self.value, [-1])))
                self.entropy = -tf.reduce_sum(self.policy * tf.log(self.policy))
                self.policy_loss = -tf.reduce_sum(tf.log(self.responsible_outputs) * self.advantages)
                self.loss = .5 * self.value_loss + self.policy_loss - self.entropy * .01

                # Local network's gradients
                local_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope)
                self.gradients = tf.gradients(self.loss, local_vars)
                self.var_norms = tf.global_norm(local_vars)
                grads, self.grad_norms = tf.clip_by_global_norm(self.gradients, 40.0)

                # Apply local gradients to global network
                global_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, GLOBAL_SCOPE)
                self.apply_grads = trainer.apply_gradients(zip(grads, global_vars))


ACNetwork = ACNetworkDgriff777


class Worker(object):
    def __init__(self, env, worker_id, trainer, global_episodes):
        self.worker_id = worker_id
        self.name = 'worker_{}'.format(worker_id)
        self.global_episodes = global_episodes
        self.env = env

        self.episode_rewards = []
        self.episode_lengths = []
        self.episode_mean_values = []

        # logging.info('env action_space: %s', env.action_space)
        # logging.info('observation space: %s', env.observation_space)

        self.local_network = ACNetwork(self.name, trainer)
        self.update_local_ops = update_target_graph(GLOBAL_SCOPE, self.name)

        self.batch_rnn_state = None

    def train(self, rollout, session, gamma, bootstrap_value):
        rollout = np.array(rollout)
        observations = np.array(rollout[:, 0].tolist(), dtype=np.float)
        actions = rollout[:, 1]
        rewards = rollout[:, 2]
        next_observations = rollout[:, 3]
        values = rollout[:, 5]

        # rewards and values from the rollout -> generate advantage and discounted returns
        # advantage function uses "Generalized Advantage Estimation"
        self.rewards_plus = np.asarray(rewards.tolist() + [bootstrap_value])
        discounted_rewards = discount(self.rewards_plus, gamma)[:-1]
        self.value_plus = np.asarray(values.tolist() + [bootstrap_value])
        advantages = rewards + gamma * self.value_plus[1:] - self.value_plus[:-1]
        advantages = discount(advantages, gamma)

        # update global network using gradients from loss
        # Generate network statistics to periodically save
        feed_dict = {self.local_network.target_value: discounted_rewards,
                     self.local_network.inputs: observations,
                     self.local_network.actions: actions,
                     self.local_network.advantages: advantages,
                     self.local_network.state_in[0]: self.batch_rnn_state[0],
                     self.local_network.state_in[1]: self.batch_rnn_state[1]}
        v_l, p_l, e_l, g_n, v_n, self.batch_rnn_state, _ = session.run([self.local_network.value_loss,
                                                                        self.local_network.policy_loss,
                                                                        self.local_network.entropy,
                                                                        self.local_network.grad_norms,
                                                                        self.local_network.var_norms,
                                                                        self.local_network.state_out,
                                                                        self.local_network.apply_grads],
                                                                       feed_dict=feed_dict)
        return v_l / len(rollout), p_l / len(rollout), e_l / len(rollout), g_n, v_n

    def work(self, session, coordinator, saver):
        episode_count = session.run(self.global_episodes)
        total_steps = 0
        print("Starting worker {}".format(self.name))
        with session.as_default(), session.graph.as_default():
            while not coordinator.should_stop():
                session.run(self.update_local_ops)
                episode_buffer = []
                episode_values = []
                episode_frames = []
                episode_reward = 0
                episode_step_count = 0
                done = False

                state = self.env.reset()
                episode_frames.append(state)

                rnn_state = self.local_network.state_init
                self.batch_rnn_state = rnn_state

                # custom
                previous_nb_lives = 3
                while not done:
                    # logging.info('predicting an action!')
                    action_dist, value, rnn_state = session.run(
                        [self.local_network.policy, self.local_network.value, self.local_network.state_out],
                        feed_dict={
                            self.local_network.inputs: [state],
                            self.local_network.state_in[0]: rnn_state[0],
                            self.local_network.state_in[1]: rnn_state[1]
                        }
                    )
                    # TODO(fkint): replace by np.random.choice([0..len(action_dist[0])], p=action_dist[0])
                    action = np.random.choice(action_dist[0], p=action_dist[0])
                    action = np.argmax(action_dist == action)
                    # logging.info('%s taking action: %s', self.name, action)
                    # logging.info('action distribution: %s', action_dist[0])
                    next_state, reward, done, info = self.env.step(action)
                    # if info['ale.lives'] < previous_nb_lives:
                    #     # reward -= 1000
                    #     previous_nb_lives = info['ale.lives']
                    # logging.info('got reward: %s', reward)
                    # logging.info('Got extra info: %s', info)
                    if not done:
                        episode_frames.append(next_state)

                    episode_buffer.append([state, action, reward, next_state, done, value[0, 0]])
                    episode_values.append(value[0, 0])

                    episode_reward += reward
                    state = next_state
                    total_steps += 1
                    episode_step_count += 1

                    if (len(episode_buffer) == MAX_EPISODE_BUFFER_LENGTH) and (not done) and (
                            episode_step_count != MAX_EPISODE_LENGTH - 1):
                        v1 = session.run(self.local_network.value, feed_dict={self.local_network.inputs: [state],
                                                                              self.local_network.state_in[0]: rnn_state[
                                                                                  0],
                                                                              self.local_network.state_in[1]: rnn_state[
                                                                                  1]})[0, 0]
                        v_l, p_l, e_l, g_n, v_n = self.train(episode_buffer, session, GAMMA, v1)
                        episode_buffer = []
                        session.run(self.update_local_ops)
                self.episode_rewards.append(episode_reward)
                self.episode_lengths.append(episode_step_count)
                self.episode_mean_values.append(np.mean(episode_values))

                if len(episode_buffer) != 0:
                    v_l, p_l, e_l, g_n, v_n = self.train(episode_buffer, session, GAMMA, 0.0)

                if self.worker_id == 0 and episode_count % 5 == 0:
                    if episode_count % 10 == 0:
                        logging.info('Saving model (episode %s) with total reward: %s', episode_count, episode_reward)
                        saver.save(session, os.path.join(MODEL_PATH, 'model-{}.cptk'.format(episode_count)))
                        logging.info('Done saving model')
                    if episode_count % 50 == 0:
                        gif_path = os.path.join(GIFS_PATH, 'episode_{}_{}.gif'.format(episode_count, episode_reward))
                        logging.info('Writing GIF to %s', gif_path)
                        make_gif(episode_frames, gif_path, len(episode_frames) * .1)

                episode_count += 1


tf.reset_default_graph()
from experiments.agents.common import make_environment

if args.mode == 'train':
    with tf.device('/cpu:0'):
        global_episodes = tf.Variable(0, dtype=tf.int32, name='global_episodes', trainable=False)
        trainer = tf.train.AdamOptimizer(learning_rate=1e-4)
        master_network = ACNetwork(GLOBAL_SCOPE, None)
        workers = [
            Worker(make_environment(GYM_GAME_NAME, clip_rewards=True, episode_life=True, scale=False, grayscale=True,
                                    interpolating=True, frame_stack=False), worker_id, trainer, global_episodes) for
            worker_id in
            range(multiprocessing.cpu_count())]
        saver = tf.train.Saver(max_to_keep=5)

    with tf.Session() as session:
        coordinator = tf.train.Coordinator()
        if LOAD_MODEL:
            logging.info('Loading model...')
            checkpoint = tf.train.get_checkpoint_state(MODEL_PATH)
            saver.restore(session, checkpoint.model_checkpoint_path)
        else:
            session.run(tf.global_variables_initializer())
        worker_threads = []
        for worker in workers:
            worker_function = lambda: worker.work(session, coordinator, saver)
            t = threading.Thread(target=worker_function)
            t.start()
            time.sleep(0.5)
            worker_threads.append(t)
        coordinator.join(worker_threads)
else:
    env = make_environment(GYM_GAME_NAME, clip_rewards=False, episode_life=False, scale=False, grayscale=True,
                           interpolating=True, frame_stack=False)

    local_network = ACNetwork(GLOBAL_SCOPE, None)
    saver = tf.train.Saver()
    with tf.Session() as sess:
        checkpoint = tf.train.get_checkpoint_state(MODEL_PATH)
        saver.restore(sess, checkpoint.model_checkpoint_path)


        def predict(obs):
            policy, value, predict.rnn_state = sess.run([
                local_network.policy,
                local_network.value, local_network.state_out],
                feed_dict={
                    local_network.inputs: [obs],
                    local_network.state_in[0]: predict.rnn_state[0],
                    local_network.state_in[1]: predict.rnn_state[1]
                })
            return np.argmax(policy)


        predict.rnn_state = local_network.state_init

        if args.mode == 'eval':
            evaluate_atari(env, predict, args.evaluation_output_file, args.nb_evaluations)
        else:
            play_atari(env, predict, visual_play=True)
