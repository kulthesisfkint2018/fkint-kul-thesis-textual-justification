import cv2
import gym
import numpy as np
from baselines import bench
from baselines import logger
from baselines.common.atari_wrappers import make_atari
from gym import spaces


class OwnWarpFrame(gym.ObservationWrapper):
    def __init__(self, env, grayscale, interpolating):
        """Warp frames to 84x84 as done in the Nature paper and later work."""
        gym.ObservationWrapper.__init__(self, env)
        self.width = 84
        self.height = 84
        self.grayscale = grayscale
        self.interpolation = cv2.INTER_AREA if interpolating else cv2.INTER_LINEAR
        self.observation_space = spaces.Box(low=0, high=255,
                                            shape=(self.height, self.width, 1 if grayscale else 3), dtype=np.uint8)

    def observation(self, frame):
        if self.grayscale:
            frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
        frame = cv2.resize(frame, (self.width, self.height), interpolation=self.interpolation)
        return frame[:, :, None] if self.grayscale else frame


def own_wrap_deepmind(env, episode_life=True, clip_rewards=True, frame_stack=False, scale=False, grayscale=False,
                      interpolating=True):
    """Configure environment for DeepMind-style Atari.
    Source: baselines.common.atari_wrappers
    """
    from baselines.common.atari_wrappers import EpisodicLifeEnv, FireResetEnv, ScaledFloatFrame, ClipRewardEnv, \
        FrameStack
    if episode_life:
        env = EpisodicLifeEnv(env)
    if 'FIRE' in env.unwrapped.get_action_meanings():
        env = FireResetEnv(env)
    env = OwnWarpFrame(env, grayscale=grayscale, interpolating=interpolating)
    if scale:
        env = ScaledFloatFrame(env)
    if clip_rewards:
        env = ClipRewardEnv(env)
    if frame_stack:
        env = FrameStack(env, 4)
    return env


def make_environment(environment_id, clip_rewards=True, episode_life=True, scale=False, grayscale=True,
                     interpolating=True, frame_stack=True):
    """Defaults are the same as wrap_deepmind's."""
    env = make_atari(environment_id)
    env = bench.Monitor(env, logger.get_dir())
    env = own_wrap_deepmind(env, frame_stack=frame_stack, scale=scale, clip_rewards=clip_rewards,
                            episode_life=episode_life,
                            grayscale=grayscale, interpolating=interpolating)
    env.seed()
    return env


def play_atari_episode(env, act, visual_play=False, verbose=False, callbacks=()):
    """

    :param env: Atari environment (with reset, step and render)
    :param act: function accepting observation and returning integer action
    :param nb_games_to_play:
    :param visual_play:
    :param verbose:
    :param callbacks:
    :return:
    """
    # print('seed before reset: ', env.seed(random.randint(0, int(1e10))))
    obs, done = env.reset(), False
    episode_rew = 0
    # print('seed after reset: ', env.seed(random.randint(0, int(1e10))))
    while not done:
        if visual_play:
            env.render()
        action = act(obs)
        obs, rew, done, extra_info = env.step(action)
        episode_rew += rew
        for c in callbacks:
            c(obs, action, rew, done, extra_info, episode_rew)
    return episode_rew


def play_atari(env, act, nb_games_to_play=None, visual_play=False, verbose=False, callbacks=()):
    all_game_rewards = []

    current_game = 0
    while nb_games_to_play is None or current_game < nb_games_to_play:
        current_game += 1
        episode_reward = play_atari_episode(env, act, visual_play=visual_play, verbose=verbose, callbacks=callbacks)
        if verbose:
            print("Episode {} reward: {}".format(current_game, episode_reward))
        if nb_games_to_play is not None:
            all_game_rewards.append(episode_reward)

    return all_game_rewards


def evaluate_atari(env, act, output_file, nb_games_to_play=100, visual_play=False):
    all_game_rewards = play_atari(env, act, nb_games_to_play=nb_games_to_play, verbose=True, visual_play=visual_play)
    with open(output_file, 'w') as f:
        f.writelines(map(lambda x: "{}\n".format(x), all_game_rewards))
