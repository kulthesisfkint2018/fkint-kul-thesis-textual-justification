import argparse

from experiments.agents.common import play_atari, make_environment, evaluate_atari
from helpers import DEFAULT_MODEL_CONFIGURATION
from helpers import model_wrapper

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--env', help='environment ID', default='MsPacmanNoFrameskip-v4')
parser.add_argument('--mode', default='play', choices=['play', 'eval'])
parser.add_argument('--nb_evaluations', default=100, type=int)
parser.add_argument('--evaluation_output_file', default='evaluation.csv', type=str)


def main():
    args = parser.parse_args()
    env = make_environment(args.env, scale=False, clip_rewards=False, episode_life=False,
                           grayscale=False, interpolating=False)
    act = model_wrapper.SimpleExtendedAgent(DEFAULT_MODEL_CONFIGURATION)
    predict = lambda x: act.predict(x)
    if args.mode == 'play':
        play_atari(env, predict, visual_play=True, verbose=True)
    else:
        evaluate_atari(env, predict, args.evaluation_output_file, args.nb_evaluations, visual_play=False)


if __name__ == "__main__":
    main()
