# Scripts
## Train models
`export NUM_TIMESTEPS=20000000`

* Naive A3C: `python -m experiments.agents.plain_a3c --env MsPacmanNoFrameskip-v0 --model_path ./data/agent_models/plain_a3c/ --gifs_path /tmp/pacman_plain_a3c_gifs/ --load_model 0`
* DQN (dueling, prioritized): `python -m experiments.agents.openai_deepq --mode train --model_path ./data/agent_models/deepq_default.pkl --env MsPacmanNoFrameskip-v0 --intermediary_models_path /tmp/intermediary-models/deepq --progress_log_file ./data/agent_models/deepq_default_progress.csv --prioritized 1 --dueling 1 --clip-rewards 1 --episode-life 1 --num-timesteps $NUM_TIMESTEPS`
* ACKTR (Pytorch, other repository): `python main.py --env-name MsPacmanNoFrameskip-v0 --algo acktr --num-processes 4 --num-steps 20 --num-frames $NUM_TIMESTEPS`
* A2C (Pytorch, other repository): `python main.py --env-name MsPacmanNoFrameskip-v0 --algo a2c --num-processes 4 --num-frames $NUM_TIMESTEPS`

## Sanity check models
* Naive A3C: check generated GIFs
* DQN: `python -m experiments.agents.openai_deepq --mode play --env MsPacmanNoFrameskip-v0 --model_path ./data/agent_models/deepq_default.pkl`
* Tensorpack A3C: `python -m experiments.agents.tensorpack_a3c --mode play --env MsPacmanNoFrameskip-v0`
* ACKTR (Pytorch, other repository): `python enjoy.py --load-dir trained_models/acktr/ --env-name MsPacmanNoFrameskip-v0`
* A2C (Pytorch, other repository): `python enjoy.py --load-dir trained_models/a2c/ --env-name MsPacmanNoFrameskip-v0`

## Evaluate models
* Naive A3C: `python -m experiments.agents.plain_a3c --mode eval --evaluation_output_file ./data/agent_models/plain_a3c_evaluation.csv --nb_evaluations 100 --model_path ./data/agent_models/plain_a3c/`
* DQN: `python -m experiments.agents.openai_deepq --mode eval --env MsPacmanNoFrameskip-v0 --model_path ./data/agent_models/deepq_default.pkl --evaluation_output_file ./data/agent_models/deepq_default_evaluation.csv --nb_evaluations 100`
* Tensorpack A3C: `python -m experiments.agents.tensorpack_a3c --mode eval --evaluation_output_file ./data/agent_models/tensorpack_a3c_evaluation.csv --nb_evaluations 100 --env MsPacmanNoFrameskip-v0`
* ACKTR (Pytorch, other repository): `python enjoy.py --load-dir trained_models/acktr/ --env-name MsPacmanNoFrameskip-v0 --num-games 100 --total_rewards_output_csv ./pytorch_acktr_evaluation.csv --no-visual`
* A2C (Pytorch, other repository): `python enjoy.py --load-dir trained_models/a2c/ --env-name MsPacmanNoFrameskip-v0 --num-games 100 --total_rewards_output_csv ./pytorch_acktr_evaluation.csv --no-visual`


# Links
* Pytorch implementations by Ilya Kostrikov.
    * Original repository: https://github.com/ikostrikov/pytorch-a2c-ppo-acktr
    * Fork: https://bitbucket.org/kulthesisfkint2018/kul-thesis-pytorch-experiments
