import argparse
import os
import time

import numpy as np
from baselines import deepq
from baselines import logger
from baselines.common import set_global_seeds

from experiments.agents.common import play_atari, make_environment, evaluate_atari


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--env', help='environment ID', default='BreakoutNoFrameskip-v4')
    parser.add_argument('--seed', help='RNG seed', type=int, default=int(time.time()))
    parser.add_argument('--prioritized', type=int, default=1)
    parser.add_argument('--dueling', type=int, default=1)
    parser.add_argument('--num-timesteps', type=int, default=int(10e6))
    parser.add_argument('--mode', type=str, choices=('train', 'play', 'eval'), default='train')
    parser.add_argument('--model_path', type=str, default='atari_model.pkl')
    parser.add_argument('--intermediary_models_path', type=str, default='/tmp/intermediary-models')
    parser.add_argument('--visual_play', type=int, default=1)
    parser.add_argument('--progress_log_file', type=str, default=None)
    parser.add_argument('--clip-rewards', type=int, default=1)
    parser.add_argument('--episode-life', type=int, default=1)
    parser.add_argument('--nb_evaluations', default=100, type=int)
    parser.add_argument('--evaluation_output_file', default='evaluation.csv', type=str)
    args = parser.parse_args()
    logger.configure()
    set_global_seeds(args.seed)

    # settings
    if args.mode in ['play', 'eval']:
        env = make_environment(args.env, clip_rewards=False, episode_life=False, interpolating=True, grayscale=True,
                               scale=True)
        act = deepq.load(args.model_path)

        def predict(ob):
            forced_ob = ob[None]
            return act(forced_ob)

        if args.mode == 'play':
            play_atari(env, predict, visual_play=bool(args.visual_play), nb_games_to_play=None, verbose=True)
        else:
            evaluate_atari(env, predict, args.evaluation_output_file, args.nb_evaluations)
        return

    env = make_environment(args.env, clip_rewards=bool(args.clip_rewards), episode_life=bool(args.episode_life), scale=True,
                           interpolating=True, grayscale=True)
    model = deepq.models.cnn_to_mlp(
        convs=[(32, 8, 4), (64, 4, 2), (64, 3, 1)],
        hiddens=[256],
        dueling=bool(args.dueling),
    )

    def learning_callback(local_vars, global_vars):
        if not local_vars['reset']:
            return False
        saved_mean_reward = local_vars['saved_mean_reward']
        if args.progress_log_file is not None:
            with open(args.progress_log_file, 'a') as f:
                mean_100ep_reward = round(np.mean(local_vars['episode_rewards'][-101:-1]))
                f.write("{},{},{}\n".format(int(time.time()), local_vars['t'], mean_100ep_reward))
        if saved_mean_reward is None:
            return False
        path = os.path.join(learning_callback.target_directory,
                            'model-progress-{0:09d}-{1:010.2f}.pkl'.format(local_vars['t'], saved_mean_reward))
        if saved_mean_reward > learning_callback.best_saved_mean_reward:
            logger.info('Saving new intermediary model (steps = {}, mean reward = {}) to {}'.format(local_vars['t'],
                                                                                                    saved_mean_reward,
                                                                                                    path))
            os.makedirs(learning_callback.target_directory, exist_ok=True)
            learning_callback.best_saved_mean_reward = saved_mean_reward
            local_vars['act'].save(path)
        return False

    learning_callback.best_saved_mean_reward = 0
    learning_callback.target_directory = args.intermediary_models_path

    act = deepq.learn(
        env,
        q_func=model,
        lr=1e-4,
        max_timesteps=args.num_timesteps,
        buffer_size=10000,
        exploration_fraction=0.1,
        exploration_final_eps=0.01,
        train_freq=4,
        learning_starts=10000,
        checkpoint_freq=10,
        target_network_update_freq=1000,
        gamma=0.99,
        print_freq=10,
        prioritized_replay=bool(args.prioritized),
        callback=learning_callback
    )
    act.save(args.model_path)
    # act.save("pong_model.pkl") XXX
    env.close()


if __name__ == '__main__':
    main()
