import matplotlib
import numpy as np
import scipy.ndimage
from scipy import stats

from helpers import DEFAULT_MODEL_CONFIGURATION
from helpers.game_constants import DIRECTION_CODES
from helpers.graphical import make_gif, get_justification_overlay_frame
from helpers.model_wrapper import SimpleExtendedAgent, get_player, InputAttributionExtendedAgent

# matplotlib.use('TkAgg')


def play_and_justify(output_file=None, overlay_fn=None, filter_fn=None, nb_frames=100, frame_interval=20,
                     first_frame_delay=75, relative_correctness_callback=None, num_samples=100,
                     action_scores_callback=None):
    agent_justification = InputAttributionExtendedAgent(DEFAULT_MODEL_CONFIGURATION, num_samples=num_samples)
    agent_normal = SimpleExtendedAgent(DEFAULT_MODEL_CONFIGURATION)

    env = get_player(train=False)
    ob = env.reset()
    sum_r = 0
    all_frames = []
    step = 0
    while True:
        step += 1
        act = agent_normal.predict(ob)
        if step > first_frame_delay and step % frame_interval == 0:
            print('Selected action: ', len(all_frames), DIRECTION_CODES[act])
            justification, justification_act, action_scores, relative_correctness = agent_justification.predict(ob)
            if relative_correctness_callback is not None:
                relative_correctness_callback(len(all_frames), act, relative_correctness, ob)
            if action_scores_callback is not None:
                action_scores_callback(action_scores, act, ob)

            new_frame = get_justification_overlay_frame(ob, justification[act]
                                                        , filter_fn=filter_fn
                                                        , overlay_fn=overlay_fn
                                                        )
            all_frames.append(new_frame)

        ob, r, isOver, info = env.step(act)
        sum_r += r
        if isOver or len(all_frames) > nb_frames:
            if output_file is not None:
                make_gif(all_frames, output_file, len(all_frames))
            break

    return


def build_overlay_fn_proportional():
    def overlay_proportional(justification):
        flat_justification = justification.flatten()

        def scale_percentile_of_score(j):
            percentile_of_score = stats.percentileofscore(flat_justification, j)
            normalized_percentile_of_score = (percentile_of_score / 100 - .5)
            return abs(normalized_percentile_of_score) * normalized_percentile_of_score * 2 + .5

        overlay = np.vectorize(scale_percentile_of_score)(
            justification)
        return overlay

    return overlay_proportional


def build_filter_fn_threshold(threshold=95):
    return lambda justification: justification > np.percentile(justification, threshold)


def build_filter_fn_area(side=3):
    plain_filter_fn = build_filter_fn_threshold(95)

    def filter_fn(justification):
        filtered = plain_filter_fn(justification)
        converted_to_nums = np.vectorize(lambda b: 1.0 if b else 0.5)(filtered)
        convolved = scipy.ndimage.uniform_filter(converted_to_nums, side)
        return np.vectorize(lambda b: 1 if b > .7 else 0)(convolved)

    return filter_fn


def build_overlay_fn_area(side=3):
    filter = build_filter_fn_area(side)

    def overlay_fn(justification):
        filtered = filter(justification)
        return np.vectorize(lambda b: 1 if b > .5 else .5)(filtered)

    return overlay_fn
