import matplotlib.pyplot as plt
import numpy as np
import os.path

from experiments.visual_attribution.common import play_and_justify

DIRNAME = './data/visual_attention/attribution_accuracy'
def get_data_points(nb_samples, nb_frames):
    print('starting: ',nb_samples)

    filename = os.path.join(DIRNAME,'{:04d}-{:03d}-accuracy-samples.npy'.format(nb_samples, nb_frames))
    alldata_filename = os.path.join(DIRNAME, '{:04d}-{:03d}-accuracy-samples-all-original.npy'.format(nb_samples, nb_frames))
    if os.path.isfile(filename):
        print('cached!')
        return np.load(filename)
    relative_correctnesses = []
    all_relative_correctnesses = []
    def report_relative_correctness(frame_nb, action, value, ob):
        print('got value: ', value)
        print('for action: ', action)
        relative_correctnesses.append(value[action])
        all_relative_correctnesses.append(value)

    play_and_justify(
        relative_correctness_callback=report_relative_correctness,
        nb_frames=nb_frames,
        num_samples=nb_samples
    )
    data = np.array(relative_correctnesses)-np.ones_like(relative_correctnesses)
    np.save(filename, data)
    np.save(alldata_filename, np.array(all_relative_correctnesses))
    return data


def main():
    NB_FRAMES = 50
    NB_SAMPLES = [
        30,
        100,
        300,
        1000,
    ]
    plt.figure()
    data = [get_data_points(nb_samples, NB_FRAMES) for nb_samples in NB_SAMPLES]
    data_labels = ["{} samples".format(nb_samples) for nb_samples in NB_SAMPLES]
    plt.boxplot(data, labels=data_labels)
    plt.xticks(rotation=45)

    plt.ylabel('relative error')
    plt.title("Accuracy of sum of attributions")
    plt.tight_layout()
    plt.show()

    plt.savefig('./data/visual_attention/attribution_accuracy.png')


if __name__ == "__main__":
    main()
