import os

import numpy as np


def count_5p(a):
    return np.count_nonzero(np.abs(a) < .05)


def report_all(nb_samples, a):
    print(nb_samples, "&", "${:0.03f}$".format(np.std(a)), "&", "${:0.03f}$".format(np.percentile(abs(a), 95)), "&",
          count_5p(a), "\\\\")


DIRNAME = 'data/visual_attention/attribution_accuracy'
FILENAME_FORMAT = '{:04d}-050-accuracy-samples.npy'
DATA = (
    30,
    100,
    300,
    1000
)


def main():
    for d in DATA:
        a = np.load(os.path.join(DIRNAME, FILENAME_FORMAT.format(d)))
        report_all(d, a)


if __name__ == "__main__":
    main()
