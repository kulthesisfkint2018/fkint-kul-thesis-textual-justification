import time

import matplotlib
import pygame

from helpers import DEFAULT_MODEL_CONFIGURATION
from helpers.game_constants import DIRECTION_CODES
from helpers.graphical import make_gif, get_justification_overlay_frame
from helpers.model_wrapper import SimpleExtendedAgent, get_player, InputAttributionExtendedAgent

matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np
from scipy import stats
import scipy.ndimage


def sample_main_play_episode():
    agent = SimpleExtendedAgent(DEFAULT_MODEL_CONFIGURATION)

    env = get_player(train=False)
    ob = env.reset()
    sum_r = 0

    while True:
        act = agent.predict(ob)
        ob, r, isOver, info = env.step(act)
        env.render()
        sum_r += r
        if isOver:
            return sum_r


def sample_main_episode_gif():
    agent_normal = SimpleExtendedAgent(DEFAULT_MODEL_CONFIGURATION)
    agent = InputAttributionExtendedAgent(DEFAULT_MODEL_CONFIGURATION, num_samples=30)

    env = get_player(train=False)
    ob = env.reset()
    sum_r = 0

    all_frames = []
    nb_steps = 0
    while True:
        nb_steps += 1
        act = agent_normal.predict(ob)
        ob, r, isOver, info = env.step(act)
        env.render()
        if nb_steps % 20 == 0:
            justification, action_scores, _, _ = agent.predict(ob)
            print("frame", len(all_frames), DIRECTION_CODES[act])
            all_frames.append(get_justification_overlay_frame(ob, justification[act]))
        sum_r += r
        print('reward: ', sum_r, 'info: ', info)
        if isOver or len(all_frames) > 100:
            make_gif(all_frames, 'exported-play-{}.gif'.format(time.time()), len(all_frames))
            return sum_r


def get_attribution_accuracy(agent, observation, action, attributions):
    zeros = np.zeros_like(observation[None, :, :, :])
    empty_and_full = np.concatenate([zeros, observation[None, :, :, :]])
    exp_policy = agent.predict(empty_and_full)
    zero_score = np.array(exp_policy)[0, action]
    final_score = np.array(exp_policy)[-1, action]
    score_increase = final_score - zero_score
    factor = attributions.shape[1]
    scaled_sum = np.sum(attributions[action]) / factor
    sum_of_attributions_over_delta_f = scaled_sum / score_increase
    return sum_of_attributions_over_delta_f


def plot_action_score_evolution_by_scaling(action_scores):
    fig = plt.figure()
    for a in DIRECTION_CODES.keys():
        plt.plot(np.linspace(0, 100, action_scores.shape[0]), action_scores[:, a], label=DIRECTION_CODES[a])
    plt.legend()
    plt.xlabel('% of state')
    plt.ylabel('action softmax score')
    plt.ylim((0, 1))
    fig.show()


def display_overlay(display, new_frame):
    surf = pygame.surfarray.make_surface(new_frame.transpose((1, 0, 2)))
    #surf = pygame.transform.scale2x(surf)
    #surf = pygame.transform.scale2x(surf)
    display.blit(surf, (0, 0))
    pygame.display.update()
    time.sleep(.001)


def init_display():
    pygame.init()
    # SCALING_FACTOR = 4
    SCALING_FACTOR =1
    display = pygame.display.set_mode((84 * 3 * SCALING_FACTOR, 84 * SCALING_FACTOR))
    return display


def sample_main_episode_show_attention():
    display = init_display()

    agent_justification = InputAttributionExtendedAgent(DEFAULT_MODEL_CONFIGURATION, num_samples=20)
    agent_normal = SimpleExtendedAgent(DEFAULT_MODEL_CONFIGURATION)

    env = get_player(train=False)
    ob = env.reset()
    sum_r = 0

    all_frames = []

    def plain_filter_fn(justification):
        return justification > np.percentile(justification, 95)

    def filter_fn(justification):
        filtered = plain_filter_fn(justification)
        converted_to_nums = np.vectorize(lambda b: 1.0 if b else 0.5)(filtered)
        convolved = scipy.ndimage.uniform_filter(converted_to_nums, 3)
        return np.vectorize(lambda b: 1 if b > .7 else 0)(convolved)

    def overlay_fn(justification):
        filtered = plain_filter_fn(justification)
        converted_to_nums = np.vectorize(lambda b: 1.0 if b else 0.5)(filtered)
        convolved = scipy.ndimage.uniform_filter(converted_to_nums, 3)
        return np.vectorize(lambda b: 1 if b > .7 else 0.2)(convolved)
    def overlay_proportional(justification):
        flat_justification = justification.flatten()
        return np.vectorize(lambda j : (stats.percentileofscore(flat_justification, j)/ 100 - .5)**4 * 16)(justification)

    step = 0
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
        step += 1
        act = agent_normal.predict(ob)
        # Justification
        if False or (step > 75 and step % 20 == 0):
            print('Selected action: ', len(all_frames), DIRECTION_CODES[act])
            justification, justification_act, action_scores, relative_correctness = agent_justification.predict(ob)

            new_frame = get_justification_overlay_frame(ob, justification[act]
                                                        #, filter_fn=filter_fn
                                                        #, overlay_fn=overlay_fn
                                                        , filter_fn=lambda x: x > np.percentile(x, 95)
                                                        , overlay_fn = overlay_proportional
                                                        )
            display_overlay(display, new_frame)
            all_frames.append(new_frame)

        ob, r, isOver, info = env.step(act)
        sum_r += r
        # print('reward: ', sum_r, 'info: ', info)
        if isOver or len(all_frames) > 100:
            make_gif(all_frames, 'exported-play-{}.gif'.format(time.time()), len(all_frames))
            break

    return


if __name__ == "__main__":
    # sample_main_play_episode()
    # sample_main_episode_gif()
    sample_main_episode_show_attention()


def export_to_mp4():
    current_frame = np.random.rand(84 * 3, 84, 3)

    fig, ax = plt.subplots()
    im = ax.imshow(current_frame, animated=True)
    im.set_data(current_frame)

    def init():  # only required for blitting to give a clean slate.
        im.set_data(current_frame)
        return [im]

    def my_update_image(i):
        current_frame = np.random.rand(84 * 3, 84, 3)
        print('updating: ', i)
        im.set_data(current_frame)
        return [im]

    ani = animation.FuncAnimation(fig, my_update_image, init_func=init, frames=100, interval=200, blit=True,
                                  save_count=50)
    ani.save('test.mp4')
