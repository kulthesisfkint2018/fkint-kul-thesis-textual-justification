from experiments.visual_attribution.common import play_and_justify, build_filter_fn_threshold, \
    build_overlay_fn_proportional


def main():
    play_and_justify('./data/visual_attention/feature_importance_pixels.gif',
                     filter_fn=build_filter_fn_threshold(95),
                     overlay_fn=build_overlay_fn_proportional())


if __name__ == "__main__":
    main()
