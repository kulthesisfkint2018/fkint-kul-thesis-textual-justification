# Visual attribution
* `feature_importance_pixel`: Highlighting pixels with high attribution.
* `feature_importance_region`: Added noise filter highlighting regions with high attribution.
* `compare_total_attribution_accuracy`: Retrieving data to compute the accuracy of the numerical approximation.
* `attribution_accuracy_reporter`: Script to generate the LaTeX table with accuracy statistics.
* `action_score_evolution`: Evolution of predicted action when scaling an image from black to the actual input.
* `main`: Example code to use different filters and export data to mp4/show using pygame.