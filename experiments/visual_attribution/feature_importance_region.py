from experiments.visual_attribution.common import play_and_justify, build_filter_fn_area, build_overlay_fn_area


def main():
    play_and_justify('./data/visual_attention/feature_importance_region.gif',
                     filter_fn=build_filter_fn_area(3),
                     overlay_fn=build_overlay_fn_area(3)
                     )


if __name__ == "__main__":
    main()
