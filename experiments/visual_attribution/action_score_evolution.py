import os
import shutil

import matplotlib.pyplot as plt
import numpy as np

from experiments.visual_attribution.common import play_and_justify
from helpers.game_constants import DIRECTION_CODES
from helpers.graphical import observation_to_image, write_np_to_image

DIRNAME = './data/visual_attention/action_score_evolution'


def main():
    sequence = 0

    def report_action_scores(action_scores, act, observation):
        nonlocal sequence
        fig = plt.figure()
        for a in DIRECTION_CODES.keys():
            plt.plot(np.linspace(0, 100, action_scores.shape[0]), action_scores[:, a], label=DIRECTION_CODES[a])
        plt.legend()
        plt.xlabel('% of state')
        plt.ylabel('action softmax score')
        plt.title('Action score evolution (chosen action = {})'.format(DIRECTION_CODES[act]))
        plt.tight_layout()
        plt.ylim((0, 1))
        fig.savefig(os.path.join(DIRNAME, '{:03d}-evolution-{}.png'.
                                 format(sequence, DIRECTION_CODES[act])))

        write_np_to_image(observation_to_image(observation),
                          os.path.join(DIRNAME, '{:03d}-observation.png'.format(sequence)))
        sequence += 1

    shutil.rmtree(DIRNAME, ignore_errors=True)
    os.mkdir(DIRNAME)
    play_and_justify(
        action_scores_callback=report_action_scores,
        nb_frames=100,
        num_samples=100
    )


if __name__ == "__main__":
    main()
