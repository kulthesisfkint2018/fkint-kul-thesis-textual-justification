# Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Image-to-text implementation based on http://arxiv.org/abs/1411.4555.
"Show and Tell: A Neural Image Caption Generator"
Oriol Vinyals, Alexander Toshev, Samy Bengio, Dumitru Erhan
"""
# Strongly based on: https://github.com/tensorflow/models/blob/master/research/im2txt/im2txt/show_and_tell_model.py
import tensorflow as tf
from tensorflow.python.estimator import model_fn as model_fn_lib

from lib.im2txt import vocabulary
from text_generation import inputs


class Model(object):
    def __init__(self, features, labels, mode, params, config):
        self.input_features = features
        self.input_labels = labels
        self.mode = mode
        self.config = config

        self.params = None
        self.set_params(params)

        self.predictions = None
        self.total_loss = None
        self.train_op = None

        self.build_model()

    def set_params(self, params):
        # TODO: verify parameter values
        self.params = params

    def is_training(self):
        return self.mode == model_fn_lib.ModeKeys.TRAIN

    def is_predicting(self):
        return self.mode == model_fn_lib.ModeKeys.PREDICT

    def get_estimator_spec(self):
        return tf.estimator.EstimatorSpec(self.mode, self.predictions, self.total_loss, self.train_op)

    @classmethod
    def get_model_fn(cls):
        def _dummy(features, labels, mode, params, config):
            return cls(features, labels, mode, params, config).get_estimator_spec()

        return _dummy

    def build_model(self):
        raise NotImplementedError

    @staticmethod
    def build_initializer(params):
        return tf.random_uniform_initializer(
            minval=-params['initializer_scale'],
            maxval=params['initializer_scale']
        )

    @staticmethod
    def build_text_embedding_map(params, initializer):
        # TODO: verify that vocabulary size matches the actual voc size
        return tf.get_variable(
            shape=[params['vocabulary_size'], params['embedding_size']],
            initializer=initializer,
            name="caption_embedding_map"
        )

    @classmethod
    def get_caption_data(cls, input_features, input_labels, voc, is_predicting):
        if is_predicting:
            return input_features["input_sequence"], None, None
        return cls.convert_caption_to_training_tensors(input_labels, voc)

    @classmethod
    def build_total_loss(cls, logits, target_caption_seq, caption_mask, params, is_predicting=False):
        if is_predicting:
            return None
        targets = tf.reshape(target_caption_seq, [-1])
        weights = tf.to_float(tf.reshape(caption_mask, [-1]))

        losses = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=targets, logits=logits)
        batch_loss = tf.div(tf.reduce_sum(tf.multiply(losses, weights)), tf.reduce_sum(weights))

        tf.losses.add_loss(batch_loss)
        cls.build_regularization(params)

        return tf.losses.get_total_loss()

    @classmethod
    def build_regularization(cls, params):
        if 'l2_regularization_factor' not in params or params['l2_regularization_factor'] is None:
            return
        tf.losses.add_loss(params['l2_regularization_factor'] * sum(
            tf.nn.l2_loss(tf_var) for tf_var in tf.trainable_variables()
        ))

    @classmethod
    def build_learning_rate_decay_fn(cls, params):
        if params["learning_rate_decay_factor"] > 0:
            num_batches_per_epoch = (params["num_examples_per_epoch"] /
                                     params["batch_size"])
            decay_steps = int(num_batches_per_epoch *
                              params["num_epochs_per_decay"])

            def _learning_rate_decay_fn(learning_rate, global_step):
                return tf.train.exponential_decay(
                    learning_rate,
                    global_step,
                    decay_steps=decay_steps,
                    decay_rate=params["learning_rate_decay_factor"],
                    staircase=True)

            return _learning_rate_decay_fn
        return None

    @classmethod
    def build_training_operation(cls, total_loss, params, is_training=True):
        if not is_training:
            return None
        learning_rate_decay_fn = cls.build_learning_rate_decay_fn(params)
        return tf.contrib.layers.optimize_loss(
            loss=total_loss,
            global_step=tf.train.get_global_step(),
            learning_rate=tf.constant(params['learning_rate']),
            optimizer=params['optimizer'],
            clip_gradients=params['clip_gradients'],
            learning_rate_decay_fn=learning_rate_decay_fn
        )

    @staticmethod
    def convert_caption_to_training_tensors(caption_input, voc):
        caption_length = tf.shape(caption_input)[1]
        caption_input_length = tf.expand_dims(tf.subtract(caption_length, 1), 0)
        caption_input_seq = tf.slice(caption_input, [0, 0], tf.concat([[-1], caption_input_length], axis=0))
        caption_target_seq = tf.slice(caption_input, [0, 1], tf.concat([[-1], caption_input_length], axis=0))

        caption_mask = tf.where(
            tf.equal(caption_input_seq, voc.end_id),
            tf.zeros(tf.shape(caption_input_seq), dtype=tf.int64),
            tf.ones(tf.shape(caption_input_seq), dtype=tf.int64))
        return caption_input_seq, caption_target_seq, caption_mask

    @staticmethod
    def build_caption_encoder(caption_embedding_map, input_caption_seq):
        return tf.nn.embedding_lookup(caption_embedding_map, input_caption_seq)

    @staticmethod
    def build_caption_decoder(lstm_outputs, params, initializer):
        with tf.variable_scope("logits") as logits_scope:
            return tf.contrib.layers.fully_connected(
                inputs=lstm_outputs, num_outputs=params['vocabulary_size'], activation_fn=None,
                weights_initializer=initializer, scope=logits_scope
            )


class TextGenerationModel(Model):
    def __init__(self, features, labels, mode, params, config):
        self.initializer = None
        self.caption_embedding_map = None
        self.initial_state_data = None
        self.lstm_previous_state = None
        self.input_caption_seq = None
        self.target_caption_seq = None
        self.caption_mask = None
        self.embedded_caption_seq = None
        self.lstm_cell = None
        self.lstm_outputs = None
        self.initial_state_tuple = None
        self.new_state_tuple = None
        self.logits = None
        super(TextGenerationModel, self).__init__(features, labels, mode, params, config)

    def build_model(self):
        # General setup
        self.initializer = self.build_initializer(self.params)
        self.caption_embedding_map = self.build_text_embedding_map(self.params, self.initializer)

        # setup of inputs
        self.initial_state_data = self.get_initial_state_data(self.input_features, self.params)
        self.lstm_previous_state = self.get_previous_state_data(self.input_features, self.params, self.is_predicting())
        self.input_caption_seq, self.target_caption_seq, self.caption_mask = \
            self.get_caption_data(
                self.input_features,
                self.input_labels,
                self.params['vocabulary'],
                is_predicting=self.is_predicting())
        self.embedded_caption_seq = self.build_caption_encoder(self.caption_embedding_map, self.input_caption_seq)

        self.lstm_cell = self.build_lstm(self.params, self.is_training())
        self.lstm_outputs, self.initial_state_tuple, self.new_state_tuple = self.build_lstm_evaluation(
            lstm_cells=self.lstm_cell, initializer=self.initializer,
            initial_state_data=self.initial_state_data,
            caption_mask=self.caption_mask,
            embedded_caption_seq=self.embedded_caption_seq,
            previous_state=self.get_previous_state_data(self.input_features, self.params, self.is_predicting()),
            is_predicting=self.is_predicting(),
            params=self.params)
        self.logits = self.build_caption_decoder(lstm_outputs=self.lstm_outputs, params=self.params,
                                                 initializer=self.initializer)

        # TODO: make predictions more general (for LSTM state passing and potentially others)
        self.predictions = self.build_predictions(self.logits, self.initial_state_tuple, self.new_state_tuple,
                                                  is_predicting=self.is_predicting())
        self.total_loss = self.build_total_loss(self.logits, self.target_caption_seq, self.caption_mask, self.params,
                                                is_predicting=self.is_predicting())
        self.train_op = self.build_training_operation(self.total_loss, self.params, self.is_training())

    @classmethod
    def build_predictions(cls, logits, initial_state_tuple, new_state_tuple, is_predicting):
        if not is_predicting:
            return None
        return {
            "initial_lstm_state": tf.concat(values=initial_state_tuple, axis=1),
            "predicted_word": tf.nn.softmax(logits),
            "new_lstm_state": tf.concat(values=new_state_tuple, axis=1)
        }

    @staticmethod
    def get_initial_state_data(features, params):
        processed_observation = features["processed_observation"]

        selected_action = features["selected_policy"]
        selected_action_onehot = tf.squeeze(tf.one_hot(selected_action, depth=9), [-2])
        initial_state_data = tf.concat(values=[
            tf.zeros_like(processed_observation) if params["ignore_observation"] else processed_observation,
            tf.zeros_like(selected_action_onehot) if params["ignore_action"] else tf.to_float(selected_action_onehot)
        ], axis=1)

        return initial_state_data

    @staticmethod
    def build_lstm(params, is_training=True):
        lstm_cells = []
        for conf in params['lstm_configuration']:
            cell = tf.nn.rnn_cell.BasicLSTMCell(num_units=conf['lstm_size'], state_is_tuple=True, reuse=tf.AUTO_REUSE)
            if is_training:
                cell = tf.contrib.rnn.DropoutWrapper(cell, input_keep_prob=conf['lstm_dropout_keep_prob'],
                                                     output_keep_prob=conf['lstm_dropout_keep_prob'])
            lstm_cells.append(cell)
        return lstm_cells

    @staticmethod
    def build_lstm_input(embedded_caption_seq, params, initializer, reuse=False):
        with tf.variable_scope("lstm_input", initializer=initializer, reuse=tf.AUTO_REUSE) as scope:
            lstm_input = embedded_caption_seq
            lstm_input = tf.contrib.layers.fully_connected(
                lstm_input, params['lstm_configuration'][0]['lstm_size'], reuse=reuse, scope=scope,
                weights_initializer=initializer,
                biases_initializer=initializer)
        return lstm_input

    @classmethod
    def build_lstm_evaluation(cls, lstm_cells, initializer, initial_state_data, caption_mask, embedded_caption_seq,
                              previous_state, params, is_predicting=False):
        with tf.variable_scope("lstm1", initializer=initializer):
            initial_state_lstm_input = tf.squeeze(
                cls.build_lstm_input(tf.zeros(tf.concat(
                    [tf.shape(initial_state_data)[:-1], [1, params['embedding_size']]], axis=0),
                    dtype=tf.float32),
                    params, initializer=initializer, reuse=tf.AUTO_REUSE),
                axis=[1])
            current_batch_size = tf.shape(initial_state_lstm_input)[0]
            if not is_predicting:
                sequence_length = tf.reduce_sum(caption_mask, 1)

            lstm_input = cls.build_lstm_input(embedded_caption_seq, params, initializer=initializer,
                                              reuse=tf.AUTO_REUSE)

            previous_lstm_initial_output = initial_state_lstm_input
            initial_lstm_states = []
            new_lstm_states = []
            lstm_outputs = lstm_input
            if is_predicting:
                lstm_outputs = tf.squeeze(lstm_outputs, axis=[1])
            for lstm_cell in lstm_cells:
                zero_state = lstm_cell.zero_state(dtype=tf.float32, batch_size=current_batch_size)
                previous_lstm_initial_output, current_lstm_initial_state = lstm_cell(previous_lstm_initial_output,
                                                                                     zero_state)
                initial_lstm_states.append(tf.concat(values=current_lstm_initial_state, axis=1))

                if is_predicting:
                    internal_state_tuple = tf.split(value=previous_state[:, 0:lstm_cell.output_size * 2],
                                                    num_or_size_splits=2, axis=1)
                    previous_state = previous_state[:, lstm_cell.output_size * 2:]
                    lstm_outputs, new_state_tuple = lstm_cell(inputs=lstm_outputs, state=internal_state_tuple)
                    new_state_tuple = tf.concat(values=new_state_tuple, axis=1)
                else:
                    lstm_outputs, _ = tf.nn.dynamic_rnn(
                        cell=lstm_cell, inputs=lstm_outputs, sequence_length=sequence_length,
                        initial_state=current_lstm_initial_state, dtype=tf.float32)
                    new_state_tuple = None
                new_lstm_states.append(new_state_tuple)

            reshaped_lstm_outputs = tf.reshape(lstm_outputs, [-1, params['lstm_configuration'][-1]['lstm_size']])
            return reshaped_lstm_outputs, initial_lstm_states, new_lstm_states

    @staticmethod
    def get_previous_state_data(input_features, params, is_predicting=True):
        if is_predicting:
            return input_features["previous_state"]
        return None


def main():
    tf.logging.set_verbosity(tf.logging.INFO)
    DATA_DIRECTORY = "../exported-labels/action-change-after-start-200-with-processed/"
    voc = vocabulary.Vocabulary(inputs.get_vocabulary_path(DATA_DIRECTORY))
    params = {
        "optimizer": "SGD",
        "clip_gradients": 5.0,
        "learning_rate": 1.0,
        "lstm_dropout_keep_prob": .7,
        "initializer_scale": 0.08,
        "embedding_size": 521,
        "vocabulary_size": 512,
        "vocabulary": voc
    }
    text_estimator = tf.estimator.Estimator(model_fn=TextGenerationModel.get_model_fn(), params=params)
    text_estimator.train(input_fn=inputs.make_training_input_fn([inputs.get_records_path(DATA_DIRECTORY)], voc),
                         steps=200)


if __name__ == "__main__":
    main()
