"""
Model based on im2txt. Src: https://github.com/tensorflow/models/tree/master/research/im2txt/im2txt
@article{Google,
author = {Google, Oriol Vinyals and Google, Alexander Toshev and Google, Samy Bengio and Google, Dumitru Erhan},
title = {{Show and Tell: A Neural Image Caption Generator}},
url = {https://arxiv.org/pdf/1411.4555.pdf}
}
"""
