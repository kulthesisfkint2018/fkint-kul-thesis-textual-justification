class TrainingConfig(object):
    def __init__(self):
        self.initial_learning_rate = 2.0

        self.max_checkpoints_to_keep = 5


class ModelConfig(object):
    def __init__(self):
        self.fc0_length = 512
