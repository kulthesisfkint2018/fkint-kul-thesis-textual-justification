import numpy as np
import tensorflow as tf

import helpers.game_constants
from text_generation import inputs
from text_generation.beam_search import BeamSearch, Sentence, WordProbability


def get_sentence_string(word_ids, voc, text_normalizer):
    return text_normalizer.output_word_separator().join([voc.id_to_word(x) for x in word_ids])


def sentence_inference_with_beam_search(text_estimator, voc, text_normalizer, example_data, params,
                                        max_sentence_length=100, word_hooks=list()):
    processed_observation_data = example_data["processed_observation"]
    selected_policy_value = int(example_data["selected_policy"][0])

    nb_candidates = 10
    best_complete_sentences = BeamSearch(nb_candidates)
    best_partial_sentences = BeamSearch(nb_candidates)

    first_result = text_estimator.predict(
        input_fn=inputs.make_initial_sentence_input_fn(
            processed_observation_data,
            selected_policy_value,
            params
        ),
        predict_keys=['initial_lstm_state']
    )
    first_result = next(first_result)
    best_partial_sentences.add_sentence(
        Sentence([WordProbability(voc.start_id, 1.)], first_result['initial_lstm_state'])
    )

    for _ in range(max_sentence_length):
        if best_partial_sentences.is_empty():
            break
        current_partial_sentences = best_partial_sentences.extract()
        result_iterator = text_estimator.predict(
            input_fn=inputs.make_multi_sentence_input_fn(
                params,
                processed_observation_data, selected_policy_value,
                [sentence.last_word_id() for sentence in current_partial_sentences],
                [sentence.hidden_state for sentence in current_partial_sentences]
            ))
        for old_sentence, result in zip(current_partial_sentences, result_iterator):
            word_probs = result['predicted_word']
            new_state = result['new_lstm_state']
            for word_id in map(int, np.argsort(word_probs)[-5:]):
                sentence = old_sentence.extend(word_id, word_probs[word_id], new_state)
                # if sentence.score() < 1. / nb_candidates:
                #     continue
                # print('new ids: ', sentence.get_word_ids())
                # print('old sentence: ', get_sentence_string(old_sentence.get_word_ids(), voc, text_normalizer),
                #       'new sentence:',
                #       get_sentence_string(sentence.get_word_ids(), voc, text_normalizer))
                if sentence.is_complete(voc):
                    best_complete_sentences.add_sentence(sentence)
                else:
                    best_partial_sentences.add_sentence(sentence)

    if best_complete_sentences.is_empty():
        # print('best complete sentences is empty!')
        if best_partial_sentences.is_empty():
            print('no partial sentences built')
        else:
            best_partial_sentence = best_partial_sentences.get_highest_n(1)[0]
            print('best partial sentence: ',
                  get_sentence_string(best_partial_sentence.get_word_ids(), voc, text_normalizer=text_normalizer))
        return None
    else:
        best_sentence = best_complete_sentences.get_highest_n(1)[0]

    # print('complete sentences: \n{}'.format(
    #     "\n".join("{}: {}".format(score, sentence) for score, sentence in sorted(
    #         [(s.score(), get_sentence_string(s.get_word_ids(), voc, text_normalizer=text_normalizer)) for s in
    #          best_sentences]))))
    return get_sentence_string(best_sentence.get_word_ids(), voc, text_normalizer=text_normalizer)


def test_sentence_inference(text_estimator, voc, text_normalizer, example_data, params, word_hooks=list()):
    processed_observation_data = example_data["processed_observation"]
    selected_policy_value = int(example_data["selected_policy"][0])
    first_result = text_estimator.predict(
        input_fn=inputs.make_initial_sentence_input_fn(processed_observation_data, selected_policy_value, params),
        predict_keys=['initial_lstm_state']
    )
    first_result = next(first_result)
    previous_state = first_result['initial_lstm_state']

    selected_word_ids = [voc.start_id]
    nb_iterations = 0
    while selected_word_ids[-1] != voc.end_id:
        nb_iterations += 1
        if nb_iterations > 100:
            tf.logging.debug('Did not stop before 100 iterations, so forcefully interrupting.')
            break
        tf.logging.info('Predicting next word...')
        result = text_estimator.predict(input_fn=inputs.make_single_sentence_input_fn(
            params,
            processed_observation_data,
            selected_policy_value,
            selected_word_ids[-1],
            previous_state
        ))
        result = next(result)
        for wh in word_hooks:
            wh(result["predicted_word"], voc, selected_word_ids)
        previous_state = np.array(result["new_lstm_state"], dtype=np.float32)
        new_word_ids = np.argsort(result["predicted_word"])[-5:]
        # new_word_id = int(np.argmax(result["predicted_word"]))
        tf.logging.info("sentence so far: %s",
                        get_sentence_string(selected_word_ids, voc, text_normalizer=text_normalizer))
        tf.logging.info("candidate next words: %s",
                        get_sentence_string(new_word_ids, voc, text_normalizer=text_normalizer))
        tf.logging.info("Next word scores: %s", result["predicted_word"][new_word_ids])
        selected_word_ids.append(int(new_word_ids[-1]))
    tf.logging.info("Action: %s (%s)", helpers.game_constants.DIRECTION_CODES[selected_policy_value],
                    selected_policy_value)
    sentence = get_sentence_string(selected_word_ids, voc, text_normalizer=text_normalizer)
    tf.logging.info("Resulting sentence: %s", sentence)
    return sentence
