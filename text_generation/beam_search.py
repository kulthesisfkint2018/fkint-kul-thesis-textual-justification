import heapq
import math


class WordProbability:
    def __init__(self, word_id, probability):
        self.word_id = word_id
        self.probability = probability

    def log_probability(self):
        try:
            return math.log(self.probability)
        except:
            return math.log(1e-8)


class Sentence:
    def __init__(self, word_probabilities, hidden_state, length_normalization_factor=0.0):
        self.word_probabilities = word_probabilities
        self.hidden_state = hidden_state
        self.length_normalization_factor = length_normalization_factor

    def __lt__(self, other):
        assert isinstance(other, Sentence)
        return self.score() < other.score()

    def __eq__(self, other):
        assert isinstance(other, Sentence)
        return self.word_probabilities == other.word_probabilities

    def score(self):
        return math.exp(sum(x.log_probability() for x in self.word_probabilities) / (
                len(self.word_probabilities) ** self.length_normalization_factor))
        # return sum(x.log_probability() for x in self.word_probabilities)

    def last_word_id(self):
        assert len(self.word_probabilities) > 0
        return self.word_probabilities[-1].word_id

    def extend(self, word_id, word_probability, new_state):
        return Sentence(self.word_probabilities + [WordProbability(word_id, word_probability)], new_state,
                        length_normalization_factor=self.length_normalization_factor)

    def is_complete(self, voc):
        return self.last_word_id() == voc.end_id

    def get_word_ids(self):
        return [wp.word_id for wp in self.word_probabilities]


class BeamSearch:
    def __init__(self, max_capacity):
        self.max_capacity = max_capacity
        self.sentences = []

    def add_sentence(self, sentence):
        if len(self.sentences) >= self.max_capacity:
            heapq.heappushpop(self.sentences, sentence)
        else:
            heapq.heappush(self.sentences, sentence)

    def get_highest_n(self, n=1):
        return heapq.nlargest(n, self.sentences)

    def extract(self):
        result = self.sentences
        self.sentences = []
        return result

    def is_empty(self):
        return len(self.sentences) == 0

    def size(self):
        return len(self.sentences)
