import string


class TextNormalizer(object):
    def __init__(self):
        self.tokenizer = None
        self.normalizer_translation = str.maketrans('', '', string.punctuation)

    def sentence_splitter(self, sentence):
        return sentence.split(self.tokenizer)

    def normalize_sentence(self, sentence):
        raw_words = self.sentence_splitter(sentence)
        return [
            word for word in [self.normalize_word(word) for word in raw_words]
            if self.is_a_word(word)
        ]

    @staticmethod
    def is_a_word(word):
        return len(word) > 0

    def normalize_word(self, word):
        return word.strip().translate(self.normalizer_translation).lower()

    @staticmethod
    def output_word_separator():
        return " "

    @classmethod
    def concatenate_normalized_words(cls, words):
        return cls.output_word_separator().join(words)


class TextCharNormalizer(TextNormalizer):
    def sentence_splitter(self, sentence):
        return list(sentence)

    def normalize_word(self, word):
        return str(word)

    @staticmethod
    def output_word_separator():
        return ""


class VocabularyBuilder(object):
    def __init__(self):
        self.word_frequencies = {}

    def feed_sentence(self, normalized_words):
        for w in normalized_words:
            self.add_word(w)

    def add_word(self, word):
        if word in self.word_frequencies:
            self.word_frequencies[word] += 1
        else:
            self.word_frequencies[word] = 1

    def build_vocabulary_list(self, max_vocab_size=512):
        all_frequencies = list(reversed(sorted(freq for w, freq in self.word_frequencies.items())))
        reduced_vocab_size = max_vocab_size - 3  # <S>, </S>, <UNK>
        if len(all_frequencies) > reduced_vocab_size:
            threshold = all_frequencies[reduced_vocab_size]
        else:
            threshold = 0

        return list(sorted([w for w, freq in self.word_frequencies.items() if freq > threshold])) + ['<S>', '</S>',
                                                                                                     '<UNK>']
