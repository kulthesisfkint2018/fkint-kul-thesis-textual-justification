import os

import numpy as np
import tensorflow as tf

from text_generation import constants


def get_vocabulary_path(data_directory):
    return os.path.join(data_directory, constants.VOCABULARY_FILENAME)


def get_records_path(data_directory):
    return os.path.join(data_directory, constants.DATA_FILENAME)


def get_validation_records_path(data_directory):
    return os.path.join(data_directory, constants.VALIDATION_DATA_FILENAME)


RECORD_CONTEXT_KEYS_TO_FEATURES = {
    "processed_observation": tf.FixedLenFeature((512), tf.float32),
    "selected_policy": tf.FixedLenFeature((1), tf.int64),
    "observation_input": tf.FixedLenFeature((12 * 84 * 84), tf.int64)
}
RECORD_SEQUENCE_KEYS_TO_FEATURES = {
    "caption_ids": tf.FixedLenSequenceFeature((), dtype=tf.int64)
}


def record_parser(record):
    context, sequence = tf.parse_single_sequence_example(record,
                                                         context_features=RECORD_CONTEXT_KEYS_TO_FEATURES,
                                                         sequence_features=RECORD_SEQUENCE_KEYS_TO_FEATURES)

    features = {
        "processed_observation": context["processed_observation"],
        "selected_policy": context["selected_policy"],
    }
    label = sequence["caption_ids"]

    return features, label


def make_training_input_fn(filenames, voc, batch_size=5):
    def _input_fn():
        dataset = tf.data.TFRecordDataset(filenames)
        dataset = dataset.map(record_parser)
        dataset = dataset.repeat(100)
        dataset = dataset.shuffle(1000)
        dataset = dataset.padded_batch(
            batch_size,
            ({"processed_observation": [512], "selected_policy": [1]}, [None]),
            ({"processed_observation": tf.to_float(0), "selected_policy": tf.to_int64(0)}, tf.to_int64(voc.end_id)))

        iterator = dataset.make_one_shot_iterator()
        features, labels = iterator.get_next()
        return features, labels

    return _input_fn


def make_single_sentence_input_fn(params, processed_observation_data, selected_policy, previous_word, previous_state):
    """
    Accepts data for 1 sample.
    :param processed_observation_data:
    :param selected_policy: int
    :param previous_word: int
    :param previous_state:
    """
    assert (len(processed_observation_data.shape) == 1)
    assert (isinstance(selected_policy, int))
    assert (isinstance(previous_word, int))
    assert (len(previous_state.shape) == 1)

    return make_raw_prediction_input_fn(
        params,
        [processed_observation_data],
        [[selected_policy]],
        [[previous_word]],
        [previous_state],
    )


def make_multi_sentence_input_fn(params, processed_observation_data, selected_policy, previous_words, previous_states):
    """
    Accepts data for 1 sample.
    :param processed_observation_data:
    :param selected_policy: int
    :param previous_words: list of int
    :param previous_states: list of state values
    """
    assert (len(previous_states) == len(previous_words))
    assert (isinstance(selected_policy, int))
    assert (len(processed_observation_data.shape) == 1)
    assert (all(len(x.shape) == 1 for x in previous_states))

    return make_raw_prediction_input_fn(
        params,
        [processed_observation_data for _ in previous_words],
        [[selected_policy] for _ in previous_words],
        [[word] for word in previous_words],
        previous_states
    )


def make_raw_prediction_input_fn(params, processed_observation_data, selected_policies, previous_words,
                                 previous_states):
    """
    Accepts raw data
    :param processed_observation_data:
    :param selected_policies:
    :param previous_words:
    :param previous_states:
    :return:
    """
    assert (len(processed_observation_data) == len(selected_policies))
    assert (len(processed_observation_data) == len(previous_words))
    assert (len(processed_observation_data) == len(previous_states))

    def _dummy_fn():
        processed_observation_value = np.array(processed_observation_data, dtype=np.float32)
        selected_policy_value = np.array(selected_policies)
        previous_state_value = np.array(previous_states, dtype=np.float32)
        input_sequence_value = np.array(previous_words)
        tf.logging.debug('processed_observation_value.shape %s', processed_observation_value.shape)
        tf.logging.debug('previous_state_value.shape %s', previous_state_value.shape)
        tf.logging.debug('input_sequence_value.shape %s', input_sequence_value.shape)
        tf.logging.debug('selected_policy_value.shape %s', selected_policy_value.shape)
        # assert len(processed_observation_value.shape) == 2
        primary_dimension = processed_observation_value.shape[0]
        # assert processed_observation_value.shape == (primary_dimension, params['observation_size'])
        # assert previous_state_value.shape == (
        #     primary_dimension, 2 * sum(conf['lstm_size'] for conf in params['lstm_configuration']))
        # assert input_sequence_value.shape == (primary_dimension, 1)
        # assert selected_policy_value.shape == (primary_dimension, 1)

        return tf.data.Dataset.from_tensors({
            "processed_observation": processed_observation_value,
            "selected_policy": selected_policy_value,
            "previous_state": previous_state_value,
            "input_sequence": input_sequence_value
        }).make_one_shot_iterator().get_next()

    return _dummy_fn


def make_initial_sentence_input_fn(processed_observation_data, selected_policy, params):
    previous_state_value = np.array([0] * (sum(conf['lstm_size'] * 2 for conf in params['lstm_configuration'])),
                                    dtype=np.float32)
    return make_single_sentence_input_fn(params, processed_observation_data, selected_policy, 0, previous_state_value)


def get_record_features(filename):
    for record in tf.python_io.tf_record_iterator(
            filename,
            options=None):
        data = record_parser(record)
        with tf.Session() as sess:
            evaluated_data = sess.run(data)
        features, label = evaluated_data
        yield features, label


def make_fake_sentences_training_input(filenames, voc, text_normalizer, batch_size, limit=-1, repeats=1):
    def training_input_fn():
        def _parse_sentence_string(line):
            normalized = text_normalizer.normalize_sentence(line.decode('utf-8'))
            result = voc.sentence_to_ids(normalized)
            return [result]

        dataset = tf.data.TextLineDataset(filenames)
        dataset = dataset.take(limit)
        dataset = dataset.map(lambda t: tf.py_func(_parse_sentence_string, [t], [tf.int64]))
        dataset = dataset.map(lambda t: ({
                                             "processed_observation": tf.random_normal([512]),
                                             "selected_policy": tf.random_uniform([1], 0, 9, dtype=tf.int64)
                                         }, t))
        dataset = dataset.repeat(repeats)
        dataset = dataset.shuffle(1000)
        dataset = dataset.padded_batch(
            batch_size,
            ({"processed_observation": [512], "selected_policy": [1]}, [None]),
            ({"processed_observation": tf.to_float(0), "selected_policy": tf.to_int64(0)}, tf.to_int64(voc.end_id)))

        iterator = dataset.make_one_shot_iterator()
        features, labels = iterator.get_next()
        return features, labels

    return training_input_fn
