import bcrypt
import click

from labelling import data_processing
from labelling.database import init_db, db_session
from labelling.models import User


@click.command()
def init_database():
    init_db()


@click.command()
@click.option('--metadata_path')
@click.option('--dataset_name')
def import_dataset(metadata_path, dataset_name):
    click.echo("Importing dataset")
    data_processing.import_dataset(dataset_name, metadata_path)


@click.command()
@click.option('--username')
@click.option('--password', prompt=True, hide_input=True, confirmation_prompt=True)
def create_user(username, password):
    click.echo("Creating user with username {}".format(username))
    user = User(username=username, password=bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt()))
    db_session.add(user)
    db_session.commit()
    click.echo("User successfully created.")


@click.command()
@click.option('--dataset_id')
@click.option('--user_id')
@click.option('--allow-incomplete/--only-complete', default=False)
@click.argument('output_file')
def export_labelled_data(dataset_id, user_id, allow_incomplete, output_file):
    click.echo("Exporting data for dataset id {} and user id {}".format(dataset_id, user_id))
    data_processing.export_labels(dataset_id, [user_id], output_file, not allow_incomplete)
