from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

import labelling.config

engine = create_engine(labelling.config.DATABASE_PATH, convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    # noinspection PyUnresolvedReferences
    import web_server.models
    Base.metadata.create_all(bind=engine)
