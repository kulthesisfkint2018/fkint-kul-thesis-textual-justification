import logging

from flask import Flask

from labelling.database import db_session

app = Flask(__name__)
app.config.from_pyfile('./config.cfg')
logging.basicConfig(level=logging.DEBUG)

# noinspection PyUnresolvedReferences
import labelling.web_server.view


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


@app.route("/")
def home():
    return "Data labelling for textual justification"
