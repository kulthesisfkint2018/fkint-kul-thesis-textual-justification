from flask import render_template, session

from labelling.models import Dataset, User
from labelling.web_server.web import app


@app.route('/datasets')
def view_datasets():
    data = Dataset.query.all()
    if 'logged_in' in session:
        current_user = User.query.get(session['user_id'])
    else:
        current_user = None
    return render_template('datasets.html', datasets=data, current_user=current_user)


@app.route('/dataset/<dataset_id>')
def view_dataset(dataset_id):
    dataset = Dataset.query.get(dataset_id)
    if 'logged_in' in session:
        current_user = User.query.get(session['user_id'])
    else:
        current_user = None
    return render_template('dataset.html', dataset=dataset, current_user=current_user)
