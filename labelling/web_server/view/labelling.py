import logging

from flask import render_template, redirect, url_for, session, request

from labelling.database import db_session
from labelling.models import DataInstance, Dataset, Label, User
from labelling.web_server.web import app


@app.route('/dataset/<int:dataset_id>/label')
def view_label_dataset(dataset_id):
    return redirect(url_for('view_label_dataset_instance', dataset_id=dataset_id, instance_id=0))


@app.route('/dataset/<int:dataset_id>/instance/<int:instance_id>/label')
def view_label_dataset_instance(dataset_id, instance_id):
    dataset = Dataset.query.get(dataset_id)
    instance = DataInstance.query.filter_by(dataset_id=dataset_id, instance_id=instance_id).first()
    if 'logged_in' in session:
        current_user = User.query.get(session['user_id'])
    else:
        current_user = None
    existing_label = Label.query.filter_by(data_instance_id=instance.id, user_id=current_user.id).first()
    return render_template('label_instance.html', data_instance=instance, dataset=dataset, label=existing_label)


@app.route('/dataset/<int:dataset_id>/instance/<int:instance_id>/label/next')
def view_label_dataset_instance_next(dataset_id, instance_id):
    next_instance_id = instance_id + 1
    next_instance = DataInstance.query.filter_by(dataset_id=dataset_id, instance_id=next_instance_id).first()
    if next_instance is None:
        return redirect(url_for('view_datasets'))
    return redirect(url_for('view_label_dataset_instance', instance_id=next_instance_id, dataset_id=dataset_id))


@app.route('/data_instance/<int:instance_id>/label', methods=['POST'])
def label_data_instance(instance_id):
    instance = DataInstance.query.get(instance_id)
    if 'logged_in' not in session:
        raise Exception('Need to be logged in')
    user = User.query.get(session['user_id'])
    existing_label = Label.query.filter_by(data_instance_id=instance_id, user_id=user.id).first()
    new_label_text = request.form['caption']
    if existing_label is not None:
        logging.info('Overwriting the existing label %s -> %s.', existing_label.label, new_label_text)
        # return Exception('Cannot re-label')
        existing_label.label = new_label_text
    else:
        new_label = Label(user_id=user.id, data_instance_id=instance_id, label=request.form['caption'])
        db_session.add(new_label)
    db_session.commit()
    return redirect(url_for('view_label_dataset_instance_next', dataset_id=instance.dataset_id,
                            instance_id=instance.instance_id))
