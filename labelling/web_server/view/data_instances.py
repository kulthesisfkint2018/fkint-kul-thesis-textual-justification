import os
import tempfile

import numpy as np
from flask import render_template, send_file, session

from helpers.graphical import split_observation, make_gif, make_black_frame
from labelling.models import DataInstance, User, Label
from labelling.web_server.web import app


@app.route('/data_instance/<data_instance_id>')
def view_data_instance(data_instance_id):
    data_instance = DataInstance.query.get(data_instance_id)
    if 'logged_in' in session:
        current_user = User.query.get(session['user_id'])
        current_user_label = Label.query.filter_by(user_id=current_user.id, data_instance_id=data_instance_id).first()
    else:
        current_user = None
        current_user_label = None
    return render_template('data_instance.html', data_instance=data_instance, current_user=current_user,
                           label=current_user_label)


@app.route('/data_instance/<int:data_instance_id>/gif')
def view_data_instance_gif(data_instance_id):
    data_instance = DataInstance.query.get(data_instance_id)
    raw_data = np.load(data_instance.raw_data_path)

    frames = split_observation(raw_data)
    black_frames = [make_black_frame(frames[0])] * 1

    tfile = tempfile.NamedTemporaryFile('w', delete=False, suffix='.gif')
    path = tfile.name
    tfile.close()
    make_gif(black_frames + frames, tfile.name, 1)
    f = open(path, 'rb')
    os.unlink(path)
    return send_file(f, attachment_filename='{:05d}.gif'.format(data_instance_id))
