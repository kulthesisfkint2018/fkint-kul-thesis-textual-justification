import bcrypt
from flask import request, render_template, redirect, url_for, session, flash

from labelling.models import User
from labelling.web_server.web import app


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password'].encode('utf8')
        user = User.query.filter_by(username=username).first()
        if user is None:
            error = 'Invalid user'
        else:
            if bcrypt.checkpw(password, user.password):
                session['logged_in'] = True
                session['user_id'] = user.id
                flash('You were logged in')
                return redirect(url_for('view_datasets'))
            else:
                error = 'Invalid password'
    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    session.pop('user_id', None)
    flash('You were logged out.')
    return redirect(url_for('view_datasets'))
