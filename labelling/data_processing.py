import json
import logging
import os
import random

import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split

from lib.im2txt import vocabulary
from text_generation import inputs
from text_generation.vocabulary_builder import VocabularyBuilder, TextNormalizer
from labelling.database import db_session
from labelling.models import Dataset, DataInstance, User, Label


def import_dataset(dataset_name, metadata_path):
    with open(metadata_path, 'r') as f:
        metadata = json.load(f)
    dataset = Dataset(name=dataset_name)
    db_session.add(dataset)
    db_session.commit()
    for record in metadata:
        logging.info('Found a record with ID: %s', record['id'])
        data_instance = DataInstance(dataset.id, int(record['id']), int(record['action']), record['raw_data_path'],
                                     record['processed_observation_path'])
        db_session.add(data_instance)
    db_session.commit()


def _floats_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))


def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def _int64s_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def _int64_feature_list(values):
    return tf.train.FeatureList(feature=[_int64_feature(value) for value in values])


def build_tf_example_from_label(label, text_normalizer, voc):
    observation_input = np.load(label.data_instance.raw_data_path)
    observation_input = np.reshape(observation_input, [12 * 84 * 84])
    selected_policy = label.data_instance.action
    processed_observation = np.load(label.data_instance.processed_observation_path)
    feature = tf.train.Features(feature={
        "processed_observation": _floats_feature(processed_observation),
        "selected_policy": _int64_feature(selected_policy),
        "observation_input": _int64s_feature(observation_input)
    })
    caption_ids = voc.sentence_to_ids(text_normalizer.normalize_sentence(label.label))
    feature_list = tf.train.FeatureLists(feature_list={
        "caption_ids": _int64_feature_list(caption_ids)
    })
    return tf.train.SequenceExample(
        context=feature,
        feature_lists=feature_list)


def build_fake_tf_example_from_label(sentence, text_normalizer, voc):
    observation_input = np.random.randint(0, 255, 12 * 84 * 84)
    selected_policy = random.randint(0, 8)
    processed_observation = 10 * np.random.randn(512)
    feature = tf.train.Features(feature={
        "processed_observation": _floats_feature(processed_observation),
        "selected_policy": _int64_feature(selected_policy),
        "observation_input": _int64s_feature(observation_input)
    })
    caption_ids = voc.sentence_to_ids(text_normalizer.normalize_sentence(sentence))
    feature_list = tf.train.FeatureLists(feature_list={
        "caption_ids": _int64_feature_list(caption_ids)
    })
    return tf.train.SequenceExample(
        context=feature,
        feature_lists=feature_list
    )


def export_fake_labels(sentences, output_directory):
    os.makedirs(output_directory, exist_ok=True)
    vocabulary_path = inputs.get_vocabulary_path(output_directory)
    data_path = inputs.get_records_path(output_directory)
    validation_path = inputs.get_validation_records_path(output_directory)

    text_normalizer = TextNormalizer()
    vocabulary_builder = VocabularyBuilder()
    for sentence in sentences:
        vocabulary_builder.feed_sentence(text_normalizer.normalize_sentence(sentence))
    with open(vocabulary_path, 'w') as f:
        f.writelines(x + "\n" for x in vocabulary_builder.build_vocabulary_list(max_vocab_size=999999))
    voc = vocabulary.Vocabulary(vocabulary_path)

    write_fake_label_examples(data_path, sentences, text_normalizer, voc)
    write_fake_label_examples(validation_path, sentences, text_normalizer, voc)


def export_labels(dataset_id, user_ids, output_directory, only_completely_labelled_datasets=True):
    os.makedirs(output_directory, exist_ok=True)
    vocabulary_path = inputs.get_vocabulary_path(output_directory)
    data_path = inputs.get_records_path(output_directory)
    validation_path = inputs.get_validation_records_path(output_directory)

    text_normalizer = TextNormalizer()
    vocabulary_builder = VocabularyBuilder()

    dataset = Dataset.query.get(dataset_id)
    all_labels = []
    for user in map(User.query.get, user_ids):
        if only_completely_labelled_datasets and not dataset.has_been_labelled_completely_by_user(user):
            logging.warning('User did not label the dataset completely: %s - %s', user.id, user.username)
            continue
        for data_instance in dataset.data_instances:
            label = Label.query.filter_by(user_id=user.id, data_instance_id=data_instance.id).first()
            if label is None:
                if only_completely_labelled_datasets:
                    logging.warning('Still found non-labelled instance!')
                continue

            logging.info('Found label for user %s, data instance %s: %s', user.username, data_instance.id, label.label)
            all_labels.append(label)

    for label in all_labels:
        vocabulary_builder.feed_sentence(text_normalizer.normalize_sentence(label.label))
    with open(vocabulary_path, 'w') as f:
        f.writelines(x + "\n" for x in vocabulary_builder.build_vocabulary_list())
    voc = vocabulary.Vocabulary(vocabulary_path)

    training_data, validation_data = train_test_split(all_labels, test_size=0.15)
    write_label_examples(data_path, training_data, text_normalizer, voc)
    write_label_examples(validation_path, validation_data, text_normalizer, voc)


def write_fake_label_examples(data_path, data, text_normalizer, voc):
    with tf.python_io.TFRecordWriter(data_path) as writer:
        for num, sentence in enumerate(data):
            if num % 100 == 0:
                print('exporting number {}'.format(num))
            example = build_fake_tf_example_from_label(sentence, text_normalizer, voc)
            writer.write(example.SerializeToString())


def write_label_examples(data_path, data, text_normalizer, voc):
    with tf.python_io.TFRecordWriter(data_path) as writer:
        for label in data:
            example = build_tf_example_from_label(label, text_normalizer, voc)
            writer.write(example.SerializeToString())
