from datetime import datetime

from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from sqlalchemy.orm import relationship, backref

from helpers.game_constants import DIRECTION_CODES
from labelling.database import Base


class Dataset(Base):
    __tablename__ = "datasets"
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)

    def __init__(self, name=None):
        self.name = name

    def has_been_labelled_completely_by_user(self, user):
        return all(i.has_been_labelled_by_user(user) for i in self.data_instances)

    def __repr__(self):
        return '<Dataset {}>'.format(self.name)


class DataInstance(Base):
    __tablename__ = "data_instances"
    id = Column(Integer, primary_key=True)

    dataset_id = Column(Integer, ForeignKey('datasets.id'), nullable=False)
    dataset = relationship('Dataset', backref=backref('data_instances', lazy=True))

    instance_id = Column(Integer)
    action = Column(Integer)
    raw_data_path = Column(String)
    processed_observation_path = Column(String, nullable=True)

    def __init__(self, dataset_id, instance_id, action, raw_data_path, processed_observation_path):
        self.dataset_id = dataset_id
        self.instance_id = instance_id
        self.action = action
        self.raw_data_path = raw_data_path
        self.processed_observation_path = processed_observation_path

    def has_been_labelled_by_user(self, user):
        return Label.query.filter_by(data_instance_id=self.id, user_id=user.id).first() is not None

    def __repr__(self):
        return '<DataInstance {}-{}>'.format(self.dataset_id, self.instance_id)

    def get_action_string(self):
        return DIRECTION_CODES[self.action]


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    username = Column(String(50), unique=True)
    password = Column(String(60))

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Label(Base):
    __tablename__ = "labels"
    id = Column(Integer, primary_key=True)

    data_instance_id = Column(Integer, ForeignKey('data_instances.id'), nullable=False)
    data_instance = relationship('DataInstance')

    user_id = Column(Integer, ForeignKey('users.id'))
    user = relationship('User')

    date = Column(DateTime, nullable=False, default=datetime.utcnow)

    label = Column(String(200))

    def __init__(self, user_id, data_instance_id, label):
        self.user_id = user_id
        self.data_instance_id = data_instance_id
        self.label = label

    def __repr__(self):
        return '<Label of data instance {} by user {}>'.format(self.data_instance_id, self.user_id)
